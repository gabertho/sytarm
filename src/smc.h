#ifndef SMC_H
#define SMC_H

#include <stdint.h>

struct smc_channel_t
{
    uint32_t
        setup,
        pulse,
        cycle,
        mode;
};

struct smc_t
{
    struct smc_channel_t chans[8]; // Only the first 4 channels are relevant
    uint32_t
        ocms,
        key1,
        key2,
        wpmr,
        wpsr;
};

#define SMC ((volatile struct smc_t*) 0x40080000)

#endif

