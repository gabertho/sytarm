/**
 *  \file   cc2500.h
 *  \brief  cc2500 driver for FR5739 board
 *  \author Tristan Delizy
 *  \from   Antoine Fraboulet, Tanguy Risset, Dominique Tournier
 *  \date   2016
 **/

/******************************************************************/
/******************************************** MODULE DESCRIPTION **/
/*
 * The cc2500 chip is connected to the FR5739 TI board on a daughter
 * board. It communicates via SPI and which pins are used is described
 * in the "I/O pins for chip control".
 * the initialisation sequence is to call init first and then configure.
 * after that sequence the device should be in idle mode, waiting to
 * be used.
 */


#ifndef CC2500_H
#define CC2500_H

#include <stdbool.h>


/******************************************************************/
/******************************************************* DEFINES **/

// CC2500 control modes
#define CC2500_IDLE_DRV_MODE    0
#define CC2500_SLEEP_DRV_MODE   1
#define CC2500_XOFF_DRV_MODE    2
#define CC2500_RX_DRV_MODE      3

// I/O pins for chip control
#define CC2500_GDO_0_PORT       4
#define CC2500_GDO_0_PIN        1
#define CC2500_GDO_2_PORT       2
#define CC2500_GDO_2_PIN        3

// error modes
#define ESUCCESS_RF             0
#define EEMPTY_RF               1
#define ERXFLOW_RF              2
#define ERXBADCRC_RF            3
#define ETXFLOW_RF              4
#define ESPI_RF                 5
#define EBADCHIP_RF             6
#define EBADCONF_RF             7
#define EBADSTATE_RF            8
#define ESYSDRV_RF              9

#define CC2500_PACKET_MAX_BUF_SIZE 256


/******************************************************************/
/********************************************************* TYPES **/

typedef void (*cc2500_cb_t) (volatile unsigned char * buffer, int size, unsigned char rssi);

typedef unsigned char BYTE;
// a configuration consumes 36 bytes of memory
typedef struct RF_SETTINGS_t {
    BYTE fsctrl1;       // Frequency synthesizer control.
    BYTE fsctrl0;       // Frequency synthesizer control.
    BYTE freq2;         // Frequency control word, high byte.
    BYTE freq1;         // Frequency control word, middle byte.
    BYTE freq0;         // Frequency control word, low byte.
    BYTE mdmcfg4;       // Modem configuration.
    BYTE mdmcfg3;       // Modem configuration.
    BYTE mdmcfg2;       // Modem configuration.
    BYTE mdmcfg1;       // Modem configuration.
    BYTE mdmcfg0;       // Modem configuration.
    BYTE channr;        // Channel number.
    BYTE deviatn;       // Modem deviation setting (when FSK modulation is enabled).
    BYTE frend1;        // Front end RX configuration.
    BYTE frend0;        // Front end TX configuration.
    BYTE mcsm0;         // Main Radio Control State Machine configuration.
    BYTE foccfg;        // Frequency Offset Compensation Configuration.
    BYTE bscfg;         // Bit synchronization Configuration.
    BYTE agcctrl2;      // AGC control.
    BYTE agcctrl1;      // AGC control.
    BYTE agcctrl0;      // AGC control.
    BYTE fscal3;        // Frequency synthesizer calibration.
    BYTE fscal2;        // Frequency synthesizer calibration.
    BYTE fscal1;        // Frequency synthesizer calibration.
    BYTE fscal0;        // Frequency synthesizer calibration.
    BYTE fstest;        // Frequency synthesizer calibration.
    BYTE test2;         // Various test settings.
    BYTE test1;         // Various test settings.
    BYTE test0;         // Various test settings.
    BYTE fifothr;       // RXFIFO and TXFIFO thresholds.
    BYTE iocfg2;        // GDO2 output pin configuration.
    BYTE iocfg0d;       // GDO0 output pin configuration. Refer to SmartRF Studio User Manual
                        // for detailed pseudo register explanation.
    BYTE pktctrl1;      // Packet automation control.
    BYTE pktctrl0;      // Packet automation control.
    BYTE addr;          // Device address.
    BYTE pktlen;        // Packet length.
} RF_SETTINGS;

// cc2500 packet structure for Sytare
struct cc2500_packet_t
{
    unsigned char buffer[CC2500_PACKET_MAX_BUF_SIZE];
};


/******************************************************************/
/************************************************ DEFAULT VALUES **/

extern const RF_SETTINGS            rfSettings_default_config;
extern const RF_SETTINGS            rfSettings_HW_reset_state;


/******************************************************************/
/****************************************************** SYSCALLS **/

void syt_cc2500_reset(void);
void syt_cc2500_init(void);
void syt_cc2500_configure(RF_SETTINGS const * cfg, int reset_state);
void syt_cc2500_set_channel(unsigned char chan);
void syt_cc2500_calibrate(void); /* puts the CC2500 in idle mode = tx/rx ready */

unsigned int syt_cc2500_send_packet(const void *buffer, size_t size); /* Tx size < 255B packet limitation  */

unsigned int syt_cc2500_idle(void); /* puts the CC2500 in idle mode = tx/rx ready */
unsigned int syt_cc2500_sleep(void); /* enter sleep mode */
unsigned int syt_cc2500_wakeup(void);
unsigned int syt_cc2500_rx_enter(void); /* Start Rx mode */
unsigned int syt_cc2500_get_drv_mode(void);

int syt_cc2500_cca(void); /* 1: busy, 0: clear */
unsigned char syt_cc2500_get_rssi(void);

void syt_cc2500_get_stored_packet(struct cc2500_packet_t *dst);


/******************************************************************/
/***************************************************** FUNCTIONS **/

void cc2500_reset(void);
unsigned int cc2500_init(void);
void cc2500_configure(RF_SETTINGS const * cfg, int reset_state);
void cc2500_drv_restore(int drv_handle, bool onboot);
void cc2500_drv_save(int drv_handle);
void cc2500_set_channel(unsigned char chan);
void cc2500_calibrate(void); /* puts the CC2500 in idle mode = tx/rx ready */

unsigned int cc2500_send_packet(const void *buffer, size_t size); /* Tx size < 63B packet limitation  */

unsigned int cc2500_idle(void); /* puts the CC2500 in idle mode = tx/rx ready */
unsigned int cc2500_sleep(void); /* enter sleep mode */
unsigned int cc2500_wakeup(void);
unsigned int cc2500_rx_enter(void); /* Start Rx mode */
unsigned int cc2500_get_drv_mode(void);

int cc2500_cca(void); /* 1: busy, 0: clear */
unsigned char cc2500_get_rssi(void);

void cc2500_get_stored_packet(struct cc2500_packet_t *dst);

#endif
