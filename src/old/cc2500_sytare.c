/**
 *  \file   cc2500.c
 *  \brief  cc2500 driver for FR5739 board
 *  \author Tristan Delizy
 *  \from   Antoine Fraboulet, Tanguy Risset, Dominique Tournier
 *  \date   2016
 **/

/******************************************************************/
/****************************************************** INCLUDES **/

#include <msp430.h>
#include <stdbool.h>
#include <drivers/utils.h>
#include <drivers/spi.h>
#include <drivers/cc2500.h>
#include <drivers/clock.h>
#include <drivers/port.h>
#include <drivers/dma.h>

 //dbg
#include "kernel.h"


/******************************************************************/
/********************************************** INTERNAL DEFINES **/

/* C2500 Registers: table 36, page 60 */
#define CC2500_REG_IOCFG2                       0x00
#define CC2500_REG_IOCFG1                       0x01
#define CC2500_REG_IOCFG0                       0x02
#define CC2500_REG_FIFOTHR                      0x03
#define CC2500_REG_SYNC1                        0x04
#define CC2500_REG_SYNC0                        0x05
#define CC2500_REG_PKTLEN                       0x06
#define CC2500_REG_PKTCTRL1                     0x07
#define CC2500_REG_PKTCTRL0                     0x08
#define CC2500_REG_ADDR                         0x09
#define CC2500_REG_CHANNR                       0x0A
#define CC2500_REG_FSCTRL1                      0x0B
#define CC2500_REG_FSCTRL0                      0x0C
#define CC2500_REG_FREQ2                        0x0D
#define CC2500_REG_FREQ1                        0x0E
#define CC2500_REG_FREQ0                        0x0F
#define CC2500_REG_MDMCFG4                      0x10
#define CC2500_REG_MDMCFG3                      0x11
#define CC2500_REG_MDMCFG2                      0x12
#define CC2500_REG_MDMCFG1                      0x13
#define CC2500_REG_MDMCFG0                      0x14
#define CC2500_REG_DEVIATN                      0x15
#define CC2500_REG_MCSM2                        0x16
#define CC2500_REG_MCSM1                        0x17
#define CC2500_REG_MCSM0                        0x18
#define CC2500_REG_FOCCFG                       0x19
#define CC2500_REG_BSCFG                        0x1A
#define CC2500_REG_AGCCTRL2                     0x1B
#define CC2500_REG_AGCCTRL1                     0x1C
#define CC2500_REG_AGCCTRL0                     0x1D
#define CC2500_REG_WOREVT1                      0x1E
#define CC2500_REG_WOREVT0                      0x1F
#define CC2500_REG_WORCTRL                      0x20
#define CC2500_REG_FREND1                       0x21
#define CC2500_REG_FREND0                       0x22
#define CC2500_REG_FSCAL3                       0x23
#define CC2500_REG_FSCAL2                       0x24
#define CC2500_REG_FSCAL1                       0x25
#define CC2500_REG_FSCAL0                       0x26
#define CC2500_REG_RCCTRL1                      0x27
#define CC2500_REG_RCCTRL0                      0x28
/* Test registers */
#define CC2500_REG_FSTEST                       0x29
#define CC2500_REG_PTEST                        0x2A
#define CC2500_REG_AGCTEST                      0x2B
#define CC2500_REG_TEST2                        0x2C
#define CC2500_REG_TEST1                        0x2D
#define CC2500_REG_TEST0                        0x2E
/* Read only registers, access with |0xc0 */
#define CC2500_REG_PARTNUM                      0x30 /* 0xF0 */
#define CC2500_REG_VERSION                      0x31 /* 0xF1 */
#define CC2500_REG_FREQEST                      0x32
#define CC2500_REG_LQI                          0x33
#define CC2500_REG_RSSI                         0x34
#define CC2500_REG_MARCSTATE                    0x35
#define CC2500_REG_WORTIME1                     0x36
#define CC2500_REG_WORTIME0                     0x37
#define CC2500_REG_PKTSTATUS                    0x38
#define CC2500_REG_VCO_VC_DAC                   0x39
#define CC2500_REG_TXBYTES                      0x3A
#define CC2500_REG_RXBYTES                      0x3B
#define CC2500_REG_RCCTRL1_STATUS               0x3C
#define CC2500_REG_RCCTRL0_STATUS               0x3D
#define CC2500_PATABLE_ADDR                     0x3E
#define CC2500_DATA_FIFO_ADDR                   0x3F

/* CC2500 RAM & register Access (table 37, 61) */
#define CC2500_REG_ACCESS_OP(x)                 (x & 0x80)
#define CC2500_REG_ACCESS_OP_READ               0x80
#define CC2500_REG_ACCESS_OP_WRITE              0x00
#define CC2500_REG_ACCESS_TYPE(x)               (x & 0x40)
#define CC2500_REG_WRITE_BURST                  (0x40)
#define CC2500_REG_READ_BURST                   (0xc0) /* cf. p. 60 datasheet */
#define CC2500_REG_ACCESS_NOBURST               0x00
#define CC2500_REG_ACCESS_ADDRESS(x)            (x & 0x3F)

/* STROBE: table 34, page 58 */
#define CC2500_STROBE_SRES                      0x30 /* reset                                       */
#define CC2500_STROBE_SFSTXON                   0x31 /* enable and calibrate                        */
#define CC2500_STROBE_SXOFF                     0x32 /* crystall off                                */
#define CC2500_STROBE_SCAL                      0x33 /* calibrate                                   */
#define CC2500_STROBE_SRX                       0x34 /* enable rx                                   */
#define CC2500_STROBE_STX                       0x35 /* enable tx                                   */
#define CC2500_STROBE_SIDLE                     0x36 /* go idle                                     */
#define CC2500_STROBE_SWOR                      0x38 /* wake on radio                               */
#define CC2500_STROBE_SPWD                      0x39 /* power down                                  */
#define CC2500_STROBE_SFRX                      0x3A /* flush Rx fifo                               */
#define CC2500_STROBE_SFTX                      0x3B /* flush Tx fifo                               */
#define CC2500_STROBE_SWORRST                   0x3C /* Reset real time clock to Event1 value.      */
#define CC2500_STROBE_SNOP                      0x3D /* no operation                                */

/* GDOx: table 33, page 54 */
#define CC2500_GDOx_RX_FIFO                     0x00 /* assert above threshold, deassert when below        */
#define CC2500_GDOx_RX_FIFO_EOP                 0x01 /* assert above threshold or EOP                      */
#define CC2500_GDOx_TX_FIFO                     0x02 /* assert above threshold, deassert below thr         */
#define CC2500_GDOx_TX_THR_FULL                 0x03 /* asserts when TX FIFO is full. De-asserts when      */
                                                     /* the TX FIFO is drained below TXFIFO_THR.           */
#define CC2500_GDOx_RX_OVER                     0x04 /* asserts when RX overflow, deassert when flushed    */
#define CC2500_GDOx_TX_UNDER                    0x05 /* asserts when RX underflow, deassert when flushed   */
#define CC2500_GDOx_SYNC_WORD                   0x06 /* assert SYNC sent/recv, deasserts on EOP            */
                                                     /* In RX, de-assert on overflow or bad address        */
                                                     /* In TX, de-assert on underflow                      */
#define CC2500_GDOx_RX_OK                       0x07 /* assert when RX PKT with CRC ok, de-assert on 1byte */
                                                     /* read from RX Fifo                                  */
#define CC2500_GDOx_PREAMB_OK                   0x08 /* assert when preamble quality reached : PQI/PQT ok  */
#define CC2500_GDOx_CCA                         0x09 /* Clear channel assessment. High when RSSI level is  */
                                                     /* below threshold (dependent on the current CCA_MODE)*/

/*PKTCTRL1 – Packet automation control*/
#define PKTCTRL1_CRC_AUTOFLUSH_MASK             0x08 /* Autoflush Rx on CRC error */
#define PKTCTRL1_APPEND_STATUS_MASK             0x04 /* Append 2 status bytes at payload */
#define PKTCTRL1_ADRR_CHECK_MASK                0x03 /* Control @ check & broadcast (00) = no @check */

/*PKTCTRL0 – Packet automation control*/
#define PKTCTRL0_WHITE_DATA_MASK                0x40 /* Data whitening */
#define PKTCTRL0_PKT_FORMAT_MASK                0x30 /* Packet format, (00) = normal mode, using TX and RX FIFOs */
#define PKTCTRL0_CC2400_EN_MASK                 0x08 /* Enable cc2400 support */
#define PKTCTRL0_CRC_EN_MASK                    0x04 /* activate CRC computation */
#define PKTCTRL0_LEN_CONF_MASK                  0x03 /* (00) = fixed // (01) = variable // (10) = infinite */

/* Driver defines */
#define CC2500_GDOx_CHIP_RDY                    0x29 /* CHIP_RDY     */
#define CC2500_GDOx_XOSC_STABLE                 0x2B /* XOSC_STABLE  */
#define CC2500_RADIO_PARTNUM                    0x80
#define CC2500_SETTING_PATABLE0                 0xFE
#define CC2500_TEST_VALUE                       0xA5
#define CC2500_FRAME_RSSI_OFFSET                0
#define CC2500_FRAME_LQI_OFFSET                 1
#define CC2500_RSSI_OFFSET                      72
#define CC2500_FIFO_THRESHOLD                   15
#define CC2500_PATABLE_VALUE                    0xFE

/* CC2500 Status */
#define CC2500_HW_STATUS_IDLE                   0x00
#define CC2500_HW_STATUS_RX                     0x01
#define CC2500_HW_STATUS_TX                     0x00
#define CC2500_HW_STATUS_FSTXON                 0x03
#define CC2500_HW_STATUS_CALIBRATE              0x04
#define CC2500_HW_STATUS_SETTLING               0x05
#define CC2500_HW_STATUS_RXFIFO_OVERFLOW        0x06
#define CC2500_HW_STATUS_TXFIFO_UNDERFLOW       0x07


/******************************************************************/
/******************************************** CHIP CONFIGURATION **/

// Chipcon
// Product = CC2500
// Chip version = E   (VERSION = 0x03)
// Crystal accuracy = 10 ppm
// X-tal frequency = 26 MHz
// RF output power = 0 dBm
// RX filterbandwidth = 541.666667 kHz
// Phase = 1
// Datarate = 249.938965 kBaud
// Modulation = (7) MSK
// Manchester enable = (0) Manchester disabled
// RF Frequency = 2432.999908 MHz
// Channel spacing = 199.951172 kHz
// Channel number = 0
// Optimization = Current
// Sync mode = (3) 30/32 sync word bits detected
// Format of RX/TX data = (0) Normal mode, use FIFOs for RX and TX
// CRC operation = (1) CRC calculation in TX and CRC check in RX enabled
// Forward Error Correction = (0) FEC disabled
// Length configuration = (1) Variable length packets, packet length configured by the first received byte after sync word.
// Packetlength = 255
// Preamble count = (2)  4 bytes
// Append status = 1
// Address check = (0) No address check
// FIFO autoflush = 0
// Device address = 0
// GDO0 signal selection = ( 6) Asserts when sync word has been sent / received, and de-asserts at the end of the packet
// GDO2 signal selection = (41) CHIP_RDY
const RF_SETTINGS rfSettings_default_config = {
    0x12,   // FSCTRL1   Frequency synthesizer control.
    0x00,   // FSCTRL0   Frequency synthesizer control.
    0x5D,   // FREQ2     Frequency control word, high byte.
    0x93,   // FREQ1     Frequency control word, middle byte.
    0xB1,   // FREQ0     Frequency control word, low byte.
    0x2D,   // MDMCFG4   Modem configuration.
    0x3B,   // MDMCFG3   Modem configuration.
    0xF3,   // MDMCFG2   Modem configuration.
    0x22,   // MDMCFG1   Modem configuration.
    0xF8,   // MDMCFG0   Modem configuration.
    0x00,   // CHANNR    Channel number.
    0x01,   // DEVIATN   Modem deviation setting (when FSK modulation is enabled).
    0xB6,   // FREND1    Front end RX configuration.
    0x10,   // FREND0    Front end TX configuration.
    0x18,   // MCSM0     Main Radio Control State Machine configuration.
    0x1D,   // FOCCFG    Frequency Offset Compensation Configuration.
    0x1C,   // BSCFG     Bit synchronization Configuration.
    0xC7,   // AGCCTRL2  AGC control.
    0x00,   // AGCCTRL1  AGC control.
    0xB0,   // AGCCTRL0  AGC control.
    0xEA,   // FSCAL3    Frequency synthesizer calibration.
    0x0A,   // FSCAL2    Frequency synthesizer calibration.
    0x00,   // FSCAL1    Frequency synthesizer calibration.
    0x11,   // FSCAL0    Frequency synthesizer calibration.
    0x59,   // FSTEST    Frequency synthesizer calibration.
    0x88,   // TEST2     Various test settings.
    0x31,   // TEST1     Various test settings.
    0x0B,   // TEST0     Various test settings.
    0x07,   // FIFOTHR   RXFIFO and TXFIFO thresholds.
    0x29,   // IOCFG2    GDO2 output pin configuration.
    0x06,   // IOCFG0D   GDO0 output pin configuration. Refer to SmartRF® Studio User Manual for detailed pseudo register explanation.
    0x08,   // PKTCTRL1  Packet automation control.
    0x45,   // PKTCTRL0  Packet automation control.
    0x00,   // ADDR      Device address.
    0xFF    // PKTLEN    Packet length.
};


const RF_SETTINGS rfSettings_HW_reset_state = {
    0x0F,   // FSCTRL1   Frequency synthesizer control.
    0x00,   // FSCTRL0   Frequency synthesizer control.
    0x3C,   // FREQ2     Frequency control word, high byte.
    0xC4,   // FREQ1     Frequency control word, middle byte.
    0xEC,   // FREQ0     Frequency control word, low byte.
    0x8C,   // MDMCFG4   Modem configuration.
    0x22,   // MDMCFG3   Modem configuration.
    0x02,   // MDMCFG2   Modem configuration.
    0x22,   // MDMCFG1   Modem configuration.
    0xF8,   // MDMCFG0   Modem configuration.
    0x00,   // CHANNR    Channel number.
    0x47,   // DEVIATN   Modem deviation setting (when FSK modulation is enabled).
    0x56,   // FREND1    Front end RX configuration.
    0x10,   // FREND0    Front end TX configuration.
    0x04,   // MCSM0     Main Radio Control State Machine configuration.
    0x36,   // FOCCFG    Frequency Offset Compensation Configuration.
    0x6C,   // BSCFG     Bit synchronization Configuration.
    0x03,   // AGCCTRL2  AGC control.
    0x40,   // AGCCTRL1  AGC control.
    0x91,   // AGCCTRL0  AGC control.
    0xA9,   // FSCAL3    Frequency synthesizer calibration.
    0x0A,   // FSCAL2    Frequency synthesizer calibration.
    0x20,   // FSCAL1    Frequency synthesizer calibration.
    0x0D,   // FSCAL0    Frequency synthesizer calibration.
    0x59,   // FSTEST    Frequency synthesizer calibration.
    0x88,   // TEST2     Various test settings.
    0x31,   // TEST1     Various test settings.
    0x0B,   // TEST0     Various test settings.
    0x07,   // FIFOTHR   RXFIFO and TXFIFO thresholds.
    0x29,   // IOCFG2    GDO2 output pin configuration.
    0x3F,   // IOCFG0D   GDO0 output pin configuration. Refer to SmartRF® Studio User Manual for detailed pseudo register explanation.
    0x04,   // PKTCTRL1  Packet automation control.
    0x05,   // PKTCTRL0  Packet automation control.
    0x00,   // ADDR      Device address.
    0xFF    // PKTLEN    Packet length.
};


/******************************************************************/
/***************************************** FUNCTIONS DECLARATION **/

/* internals and low level functions */
void cc2500_spi_tx(unsigned char tx_byte);
void cc2500_spi_strobe(unsigned char strobe);
void cc2500_spi_write_reg(unsigned char reg_adr, unsigned char reg_val);
unsigned char cc2500_spi_read_reg(unsigned char reg_adr);
unsigned char cc2500_spi_read_ro_reg(unsigned char reg_adr);
void cc2500_spi_tx_fifo_byte(unsigned char tx_fifo_byte);
void cc2500_spi_tx_fifo_burst(const void *val, size_t len);
void cc2500_spi_rx_fifo_burst(void *val, size_t len);
unsigned char cc2500_spi_rx_fifo_byte();
void cc2500_check_fifo_xflow_flush(void);
void cc2500_wait_hw_status(unsigned char state);
unsigned int cc2500_check_tx_underflow(void);
unsigned int cc2500_check_rx_underflow(void);

/* utils and config internal functions */
void cc2500_set_channel(unsigned char chan);
void cc2500_calibrate(void);
void cc2500_gdo0_set_signal(unsigned char signal);
void cc2500_gdo2_set_signal(unsigned char signal);
void cc2500_set_fifo_threshold(unsigned char thr);

/* cc2500 operations */
void cc2500_rx_pkt_eop           (void);

void cc2500_drv_on_interrupt(unsigned long event);

/******************************************************************/
/********************************************************* TYPES **/

// sytare RF driver data descriptor for persistency
struct cc2500_device_context_t
{
    unsigned int                control_mode;
    RF_SETTINGS                 settings;
    struct cc2500_packet_t      packet;
};


/******************************************************************/
/***************************************************** VARIABLES **/

/* CC2500 status byte informations */
/* [7 chip ready][6:4 cc2500_status][3:0 fifo_bytes_available] */
static
unsigned char cc2500_status_register;

static
syt_dev_ctx_changes_t ctx_changes;

// local copy in RAM
static
struct cc2500_device_context_t cc2500_device_context;

static bool driver_registered = false;


/******************************************************************/
/********************************************** INLINE FUNCTIONS **/

static inline void cc2500_flush_rx(void)                            { cc2500_spi_strobe(CC2500_STROBE_SFRX); }
static inline void cc2500_flush_tx(void)                            { cc2500_spi_strobe(CC2500_STROBE_SFTX); }
static inline void cc2500_spi_enable(void)                          { spi_select_slave(SPI_DEV_CC2500); }
static inline void cc2500_spi_disable(void)                         { spi_deselect_slave(); }
static inline unsigned int cc2500_check_miso_high(void)             { return spi_is_recieving(); }
static inline void cc2500_disable_gdo_0_interrupts(void)            { prt_ie_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN)); }
static inline void cc2500_disable_gdo_2_interrupts(void)            { prt_ie_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN)); }
static inline void cc2500_enable_gdo_0_interrupts(void)             { prt_ie_bis(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN)); }
static inline void cc2500_enable_gdo_2_interrupts(void)             { prt_ie_bis(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN)); }
static inline void cc2500_clear_gdo_0_int_flag(void)                { prt_ifg_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN)); }
static inline void cc2500_clear_gdo_2_int_flag(void)                { prt_ifg_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN)); }
static inline void cc2500_update_status(void)                       { cc2500_spi_strobe(CC2500_STROBE_SNOP);}
static inline unsigned char cc2500_get_hw_state_from_status(void)   { return ((cc2500_status_register >> 4) & 0x07); }


/******************************************************************/
/******************************************* LOW LEVEL FUNCTIONS **/

void cc2500_spi_tx(unsigned char tx_byte)
{
    spi_transfert_byte(&tx_byte, &cc2500_status_register);
}

void cc2500_spi_strobe(unsigned char strobe)
{
    cc2500_spi_enable();
    cc2500_spi_tx(strobe | CC2500_REG_ACCESS_OP_WRITE | CC2500_REG_ACCESS_NOBURST);
    cc2500_spi_disable();
}

void cc2500_spi_write_reg(unsigned char reg_adr, unsigned char reg_val)
{
    cc2500_spi_enable();
    cc2500_spi_tx(reg_adr | CC2500_REG_ACCESS_OP_WRITE | CC2500_REG_ACCESS_NOBURST);
    cc2500_spi_tx(reg_val);
    cc2500_spi_disable();
}

unsigned char cc2500_spi_read_reg(unsigned char reg_adr)
{
    unsigned char r;
    cc2500_spi_enable();
    cc2500_spi_tx(reg_adr | CC2500_REG_ACCESS_OP_READ | CC2500_REG_ACCESS_NOBURST);
    spi_read_byte(&r);
    cc2500_spi_disable();
    return r;
}

unsigned char cc2500_spi_read_ro_reg(unsigned char reg_adr)
{
    unsigned char r;
    cc2500_spi_enable();
    cc2500_spi_tx(reg_adr | CC2500_REG_ACCESS_OP_READ | CC2500_REG_READ_BURST);
    spi_read_byte(&r);
    cc2500_spi_disable();
    return r;
}

void cc2500_spi_tx_fifo_byte(unsigned char tx_fifo_byte)
{
    cc2500_spi_enable();
    cc2500_spi_tx(CC2500_DATA_FIFO_ADDR);
    cc2500_spi_tx(tx_fifo_byte);
    cc2500_spi_disable();
}

void cc2500_spi_tx_fifo_burst(const void *val, size_t len)
{
    const uint8_t *ptr = val;
    cc2500_spi_enable();
    cc2500_spi_tx(CC2500_DATA_FIFO_ADDR | CC2500_REG_WRITE_BURST);
    while(len--)
        cc2500_spi_tx(*ptr++);
    cc2500_spi_disable();
}

void cc2500_spi_rx_fifo_burst(void *val, size_t len)
{
    uint8_t *ptr = val;
    cc2500_spi_enable();
    cc2500_spi_tx(CC2500_DATA_FIFO_ADDR | CC2500_REG_ACCESS_OP_READ | CC2500_REG_READ_BURST);
    while(len--)
        spi_read_byte(ptr++);
    cc2500_spi_disable();
}

unsigned char cc2500_spi_rx_fifo_byte()
{
    unsigned char r;
    cc2500_spi_enable();
    cc2500_spi_tx(CC2500_DATA_FIFO_ADDR | CC2500_REG_ACCESS_OP_READ);
    spi_read_byte(&r);
    cc2500_spi_disable();
    return r;
}

unsigned int cc2500_check_tx_underflow(void)
{
    cc2500_update_status();
    return (cc2500_get_hw_state_from_status() == CC2500_HW_STATUS_TXFIFO_UNDERFLOW);
}

unsigned int cc2500_check_rx_overflow(void)
{
    cc2500_update_status();
    return (cc2500_get_hw_state_from_status() == CC2500_HW_STATUS_RXFIFO_OVERFLOW);
}


/******************************************************************/
/********************************************** RX/TX OPERATIONS **/

unsigned int cc2500_send_packet(const void *buffer, size_t size)
{
    /* we only can TX when we are in idle, and state transition must be explicit.*/
    if (cc2500_get_drv_mode()!=CC2500_IDLE_DRV_MODE)
        return EBADSTATE_RF;

    const uint8_t *buf = buffer;
    int smallpacket = (size + 1 < 256); // Additional byte for packet length

    if(smallpacket)
    {
        int first_frame = 1;
        size_t txbytes = 1;
        cc2500_flush_tx(); 
        /* Fill tx fifo /!\ variable packet length usage. */
        cc2500_spi_tx_fifo_byte(size);
        do
        {
            size_t to_write = 64-txbytes;
            to_write = (size < to_write ? size : to_write);

            cc2500_spi_tx_fifo_burst(buf, to_write);
            if(first_frame)
                cc2500_spi_strobe(CC2500_STROBE_STX);
            buf += to_write;
            size -= to_write;
            first_frame = 0;

            /* Active wait until TX fifo is half empty */
            do {
                txbytes = (cc2500_spi_read_ro_reg(CC2500_REG_TXBYTES) & 0x7f);
            } while(txbytes > 60);
        } while(size);

        do {
            txbytes = (cc2500_spi_read_ro_reg(CC2500_REG_TXBYTES) & 0x7f);
        } while(txbytes > 0);
        while((cc2500_spi_read_ro_reg(CC2500_REG_MARCSTATE) & 0x1f) != 0x01);
    }
    else
    {
    }

    return ESUCCESS_RF;
}

void cc2500_rx_pkt_eop(void) /* called from IRQ context */
{
    /* GB-2017-05-22: Since this routine is called when a packet is complete,
     * there is no need to wait anymore. This could be a problem in the case of infitine length packets (SWRS040C, section 20 "Data FIFO"). But since we don't
     * support them yet, it works.
     * See my comment on cc2500_io_lines_init to get more details.
     */

    size_t first_frame = 1;
    size_t remaining = 0;
    uint8_t *ptr = cc2500_device_context.packet.buffer;

    do {
        uint8_t rxbytes = cc2500_spi_read_ro_reg(CC2500_REG_RXBYTES);
        uint8_t l;
        do {
            l = rxbytes;
            rxbytes = cc2500_spi_read_ro_reg(CC2500_REG_RXBYTES);
        } while (rxbytes != l);

        if(rxbytes & 0x80) // RX overflow
            break;

        rxbytes &= 0x7f;

        if(first_frame) {
            // First byte == packet length
            remaining = cc2500_spi_rx_fifo_byte();
            --rxbytes;
            first_frame = 0;
        }

        size_t to_read = (rxbytes <= remaining ? rxbytes : remaining);
        if(to_read == 0 || to_read > 64) // Issue
            break;
        cc2500_spi_rx_fifo_burst(ptr, to_read);
        ptr += to_read;
        remaining -= to_read;

    } while(remaining);

    cc2500_flush_rx();

    cc2500_clear_gdo_0_int_flag();
    cc2500_clear_gdo_2_int_flag();

    if(remaining) { // A problem occurred
        // Error?
    } else {
        //rx_streamer(cc2500_rx_packet, actualSize);
    }

    // TODO: Why do the operations above require RX mode to be enabled again?
    cc2500_idle();
    cc2500_rx_enter();
}


/******************************************************************/
/********************************************************* UTILS **/

unsigned char cc2500_packet_status(void)
{
    unsigned char ps;
    ps = cc2500_spi_read_ro_reg(CC2500_REG_PKTSTATUS);
    return ps;
}

int cc2500_cca(void)
{
    unsigned char cca = cc2500_packet_status();
    return (cca >> 4) & 0x01;
}

unsigned char cc2500_get_rssi(void)
{
    unsigned char rssi;
    /* cc2500 should be in Rx */
    rssi = cc2500_spi_read_ro_reg(CC2500_REG_RSSI);
    return rssi;
}

void cc2500_check_fifo_xflow_flush(void)
{
  cc2500_update_status();
  if (cc2500_check_tx_underflow()){
      cc2500_flush_tx();
  }
  if (cc2500_check_rx_overflow()){
      cc2500_flush_rx();
  }
}

void cc2500_wait_hw_status(unsigned char state)
{
    unsigned char s;
    do {
        cc2500_update_status();
        s = cc2500_get_hw_state_from_status();
    } while (s != state);
}

unsigned int cc2500_get_drv_mode(void)
{
    return cc2500_device_context.control_mode;
}

void cc2500_set_channel(unsigned char chan)
{
    cc2500_spi_write_reg(CC2500_REG_CHANNR, chan);
    cc2500_device_context.settings.channr = chan;

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
}

void cc2500_calibrate(void)
{
    cc2500_idle();
    cc2500_spi_strobe(CC2500_STROBE_SCAL);
    cc2500_wait_hw_status(CC2500_HW_STATUS_IDLE);
}

void cc2500_gdo0_set_signal(unsigned char signal)
{
    unsigned char reg = signal & 0x3F;
    cc2500_spi_write_reg(CC2500_REG_IOCFG0, reg);

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
}

void cc2500_gdo2_set_signal(unsigned char signal)
{
    unsigned char reg = signal & 0x3F;
    cc2500_spi_write_reg(CC2500_REG_IOCFG2, reg);

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
}

void cc2500_set_fifo_threshold(unsigned char thr)
{
    unsigned char reg;
    reg  = (thr & 0x0F);
    cc2500_spi_write_reg(CC2500_REG_FIFOTHR, reg);
    cc2500_device_context.settings.fifothr = reg;

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
}


/******************************************************************/
/************************************ STATE TRANSITION FUNCTIONS **/

/* idle mode
 * - wait for idle
 */
static unsigned int __attribute__((noinline)) cc2500_idle_hw(void)
{
    cc2500_disable_gdo_0_interrupts();
    cc2500_disable_gdo_2_interrupts();
    cc2500_check_fifo_xflow_flush();
    cc2500_spi_strobe(CC2500_STROBE_SIDLE);
    cc2500_wait_hw_status(CC2500_HW_STATUS_IDLE);
    cc2500_device_context.control_mode = CC2500_IDLE_DRV_MODE;
    return ESUCCESS_RF;
}

unsigned int cc2500_idle(void)
{
    unsigned int ret = cc2500_idle_hw();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}

/* sleep mode
 * - crystal is off
 * - configuration saved except power table and test registers
 * - calibration and power table update needed after sleep
 */
static unsigned int __attribute__((noinline)) cc2500_sleep_hw(void)
{
    cc2500_spi_strobe(CC2500_STROBE_SPWD);
    cc2500_device_context.control_mode = CC2500_SLEEP_DRV_MODE;
    return ESUCCESS_RF;
}

unsigned int cc2500_sleep(void)
{
    unsigned int ret = cc2500_sleep_hw();
    
    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ret;
}

static unsigned int __attribute__((noinline)) cc2500_wakeup_hw(void)
{
    /* We can only wakeup from SLEEP mode*/
    if (cc2500_get_drv_mode()!=CC2500_SLEEP_DRV_MODE)
        return EBADSTATE_RF;

    cc2500_spi_enable();
    /* wait for MISO to go high indicating the oscillator is stable */
    while (cc2500_check_miso_high());
    /* wakeup is complete, drive CSn high and continue */
    cc2500_spi_disable();
    cc2500_idle();
    cc2500_wait_hw_status(CC2500_HW_STATUS_IDLE);
    cc2500_device_context.control_mode = CC2500_IDLE_DRV_MODE;

    return ESUCCESS_RF;
}

unsigned int cc2500_wakeup(void)
{
    unsigned int ret = cc2500_wakeup_hw();
    
    // signal the state modification to the os
    if(ret == ESUCCESS_RF)
        drv_mark_dirty(&ctx_changes);

    return ret;
}

unsigned int cc2500_rx_enter(void)
{
    /* We can only enter RX from IDLE mode, and state transition must be explicit*/
    if (cc2500_get_drv_mode()!=CC2500_IDLE_DRV_MODE)
        return EBADSTATE_RF;

    cc2500_idle();
    cc2500_clear_gdo_0_int_flag();            /* clear pending irq     */
    cc2500_clear_gdo_2_int_flag();            /* clear pending irq     */
    cc2500_enable_gdo_0_interrupts();
    cc2500_disable_gdo_2_interrupts();
    cc2500_spi_strobe(CC2500_STROBE_SRX);
    cc2500_wait_hw_status(CC2500_HW_STATUS_RX);
    cc2500_device_context.control_mode = CC2500_RX_DRV_MODE;

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
    return ESUCCESS_RF;
}


/******************************************************************/
/********************************************* INIT CONF & RESET **/

void cc2500_reset(void)
{
    cc2500_spi_strobe(CC2500_STROBE_SRES);
    cc2500_wait_hw_status(CC2500_HW_STATUS_IDLE);
    cc2500_device_context.control_mode = CC2500_IDLE_DRV_MODE;

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
}

void cc2500_HW_init(void)
{
// Power on reset sequence ---------------------------------------//
    /* pulse CSn low then high */
    cc2500_spi_enable();
    cc2500_spi_disable();
    /* hold CSn high for at least 40 microseconds */
    clk_delay_micro(40);

    /* pull CSn low and wait for SO to go low */
    cc2500_spi_enable();

    /* directly send strobe command*/
    cc2500_spi_tx( CC2500_STROBE_SRES );

    /* return CSn pin to its default high level */
    cc2500_spi_disable();

    /* send a first empty command */
    cc2500_spi_strobe( CC2500_STROBE_SNOP );
}

void cc2500_io_lines_init(void)
{
    // IO lines Configuration ----------------------------------------//

    /* GDO0 asserted when rx fifo above threshold 
     * Input, pulldown
     */
    cc2500_gdo0_set_signal(CC2500_GDOx_RX_FIFO_EOP);
    prt_sel0_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    prt_sel1_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    prt_dir_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    prt_ie_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    prt_ren_bis(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    prt_out_bic(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    /* GB-2017-05-22: I put bis instead of bic as high-to-low transition means
     * that the received packet is complete
     * (SWRS040C, table 33 "GDOx Signal Selection", description of value 0x06).
     * Using bic would force us to wait
     * until the packet is complete, and we are not able to do that in a
     * flawless way.
     */
    prt_ies_bis(CC2500_GDO_0_PORT, BITX(CC2500_GDO_0_PIN));
    cc2500_disable_gdo_0_interrupts();

    /* GDO2 Deasserted when packet rx/tx or fifo xxxflow 
     * Input, pulldown
     */
    cc2500_gdo2_set_signal(CC2500_GDOx_SYNC_WORD);
    prt_sel0_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    prt_sel1_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    prt_dir_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    prt_ie_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    prt_ren_bis(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    prt_out_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    //prt_ies_bic(CC2500_GDO_2_PORT, BITX(CC2500_GDO_2_PIN));
    cc2500_disable_gdo_2_interrupts();

    cc2500_set_fifo_threshold(CC2500_FIFO_THRESHOLD);
}

void __attribute__((noinline)) cc2500_init_hw(void)
{
    // Power on reset sequence ---------------------------------------//
    cc2500_HW_init();

    // IO lines Configuration ----------------------------------------//
    cc2500_io_lines_init();

    /* ensure we are in idle mode at the end of init */
    cc2500_idle();
}

unsigned int cc2500_init(void)
{
    syt_drv_register(cc2500_drv_save, cc2500_drv_restore,
#ifdef SYT_ES_RX
                     cc2500_drv_on_interrupt,
#else
                     0,
#pragma message("No cc2500 ISR")
#endif
                     sizeof(struct cc2500_device_context_t));

    driver_registered = true;

    cc2500_init_hw();

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);

    return ESUCCESS_RF;
}

static void __attribute__((noinline)) cc2500_configure_hw(RF_SETTINGS const * cfg, int reset_state)
{
    // GS-2016-07-22-14:27 TODO: rename and/or comment this variable
    // so that it makes more sense for the reader. does "comp" stand for "compute" ? "compare" ?
    RF_SETTINGS const * comp_cfg;
    if(reset_state)
        comp_cfg = &rfSettings_HW_reset_state;
    else
        comp_cfg = &(cc2500_device_context.settings);


    /* Configure the CC2500 registers via SPI CMD */
    if(cfg->fsctrl1 != comp_cfg->fsctrl1){
        cc2500_spi_write_reg( CC2500_REG_FSCTRL1,  cfg->fsctrl1  );
    }
    cc2500_device_context.settings.fsctrl1 = cfg->fsctrl1;

    if(cfg->fsctrl0 != comp_cfg->fsctrl0){
        cc2500_spi_write_reg( CC2500_REG_FSCTRL0,  cfg->fsctrl0  );
    }
    cc2500_device_context.settings.fsctrl0 = cfg->fsctrl0;

    if(cfg->freq2 != comp_cfg->freq2){
        cc2500_spi_write_reg( CC2500_REG_FREQ2,  cfg->freq2  );
    }
        cc2500_device_context.settings.freq2 = cfg->freq2;
    if(cfg->freq1 != comp_cfg->freq1){
        cc2500_spi_write_reg( CC2500_REG_FREQ1,  cfg->freq1  );
    }
        cc2500_device_context.settings.freq1 = cfg->freq1;
    if(cfg->freq0 != comp_cfg->freq0){
        cc2500_spi_write_reg( CC2500_REG_FREQ0,  cfg->freq0  );
    }
        cc2500_device_context.settings.freq0 = cfg->freq0;
    if(cfg->mdmcfg4 != comp_cfg->mdmcfg4){
        cc2500_spi_write_reg( CC2500_REG_MDMCFG4,  cfg->mdmcfg4  );
    }
        cc2500_device_context.settings.mdmcfg4 = cfg->mdmcfg4;
    if(cfg->mdmcfg3 != comp_cfg->mdmcfg3){
        cc2500_spi_write_reg( CC2500_REG_MDMCFG3,  cfg->mdmcfg3  );
    }
        cc2500_device_context.settings.mdmcfg3 = cfg->mdmcfg3;
    if(cfg->mdmcfg2 != comp_cfg->mdmcfg2){
        cc2500_spi_write_reg( CC2500_REG_MDMCFG2,  cfg->mdmcfg2  );
    }
        cc2500_device_context.settings.mdmcfg2 = cfg->mdmcfg2;
    if(cfg->mdmcfg1 != comp_cfg->mdmcfg1){
        cc2500_spi_write_reg( CC2500_REG_MDMCFG1,  cfg->mdmcfg1  );
    }
        cc2500_device_context.settings.mdmcfg1 = cfg->mdmcfg1;
    if(cfg->mdmcfg0 != comp_cfg->mdmcfg0){
        cc2500_spi_write_reg( CC2500_REG_MDMCFG0,  cfg->mdmcfg0  );
    }
        cc2500_device_context.settings.mdmcfg0 = cfg->mdmcfg0;
    if(cfg->channr != comp_cfg->channr){
        cc2500_spi_write_reg( CC2500_REG_CHANNR,  cfg->channr  );
    }
        cc2500_device_context.settings.channr = cfg->channr;
    if(cfg->deviatn != comp_cfg->deviatn){
        cc2500_spi_write_reg( CC2500_REG_DEVIATN,  cfg->deviatn  );
    }
        cc2500_device_context.settings.deviatn = cfg->deviatn;
    if(cfg->frend1 != comp_cfg->frend1){
        cc2500_spi_write_reg( CC2500_REG_FREND1,  cfg->frend1  );
    }
        cc2500_device_context.settings.frend1 = cfg->frend1;
    if(cfg->frend0 != comp_cfg->frend0){
        cc2500_spi_write_reg( CC2500_REG_FREND0,  cfg->frend0  );
    }
        cc2500_device_context.settings.frend0 = cfg->frend0;
    if(cfg->mcsm0 != comp_cfg->mcsm0){
        cc2500_spi_write_reg( CC2500_REG_MCSM0,  cfg->mcsm0  );
    }
        cc2500_device_context.settings.mcsm0 = cfg->mcsm0;
    if(cfg->foccfg != comp_cfg->foccfg){
        cc2500_spi_write_reg( CC2500_REG_FOCCFG,  cfg->foccfg  );
    }
        cc2500_device_context.settings.foccfg = cfg->foccfg;
    if(cfg->bscfg != comp_cfg->bscfg){
        cc2500_spi_write_reg( CC2500_REG_BSCFG,  cfg->bscfg  );
    }
        cc2500_device_context.settings.bscfg = cfg->bscfg;
    if(cfg->agcctrl2 != comp_cfg->agcctrl2){
        cc2500_spi_write_reg( CC2500_REG_AGCCTRL2,  cfg->agcctrl2  );
    }
        cc2500_device_context.settings.agcctrl2 = cfg->agcctrl2;
    if(cfg->agcctrl1 != comp_cfg->agcctrl1){
        cc2500_spi_write_reg( CC2500_REG_AGCCTRL1,  cfg->agcctrl1  );
    }
        cc2500_device_context.settings.agcctrl1 = cfg->agcctrl1;
    if(cfg->agcctrl0 != comp_cfg->agcctrl0){
        cc2500_spi_write_reg( CC2500_REG_AGCCTRL0,  cfg->agcctrl0  );
    }
        cc2500_device_context.settings.agcctrl0 = cfg->agcctrl0;
    if(cfg->fscal3 != comp_cfg->fscal3){
        cc2500_spi_write_reg( CC2500_REG_FSCAL3,  cfg->fscal3  );
    }
        cc2500_device_context.settings.fscal3 = cfg->fscal3;
    if(cfg->fscal2 != comp_cfg->fscal2){
        cc2500_spi_write_reg( CC2500_REG_FSCAL2,  cfg->fscal2  );
    }
        cc2500_device_context.settings.fscal2 = cfg->fscal2;
    if(cfg->fscal1 != comp_cfg->fscal1){
        cc2500_spi_write_reg( CC2500_REG_FSCAL1,  cfg->fscal1  );
    }
        cc2500_device_context.settings.fscal1 = cfg->fscal1;
    if(cfg->fscal0 != comp_cfg->fscal0){
        cc2500_spi_write_reg( CC2500_REG_FSCAL0,  cfg->fscal0  );
    }
        cc2500_device_context.settings.fscal0 = cfg->fscal0;
    if(cfg->fstest != comp_cfg->fstest){
        cc2500_spi_write_reg( CC2500_REG_FSTEST,  cfg->fstest  );
    }
        cc2500_device_context.settings.fstest = cfg->fstest;
    if(cfg->test2 != comp_cfg->test2){
        cc2500_spi_write_reg( CC2500_REG_TEST2,  cfg->test2  );
    }
        cc2500_device_context.settings.test2 = cfg->test2;
    if(cfg->test1 != comp_cfg->test1){
        cc2500_spi_write_reg( CC2500_REG_TEST1,  cfg->test1  );
    }
        cc2500_device_context.settings.test1 = cfg->test1;
    if(cfg->test0 != comp_cfg->test0){
        cc2500_spi_write_reg( CC2500_REG_TEST0,  cfg->test0  );
    }
        cc2500_device_context.settings.test0 = cfg->test0;
    if(cfg->fifothr != comp_cfg->fifothr){
        cc2500_spi_write_reg( CC2500_REG_FIFOTHR,  cfg->fifothr  );
    }
        cc2500_device_context.settings.fifothr = cfg->fifothr;
    if(cfg->iocfg2 != comp_cfg->iocfg2){
        cc2500_spi_write_reg( CC2500_REG_IOCFG2,  cfg->iocfg2  );
    }
        cc2500_device_context.settings.iocfg2 = cfg->iocfg2;
    if(cfg->iocfg0d != comp_cfg->iocfg0d){
        cc2500_spi_write_reg( CC2500_REG_IOCFG0,  cfg->iocfg0d  );
    }
        cc2500_device_context.settings.iocfg0d = cfg->iocfg0d;
    if(cfg->pktctrl1 != comp_cfg->pktctrl1){
        cc2500_spi_write_reg( CC2500_REG_PKTCTRL1,  cfg->pktctrl1  );
    }
        cc2500_device_context.settings.pktctrl1 = cfg->pktctrl1;
    if(cfg->pktctrl0 != comp_cfg->pktctrl0){
        cc2500_spi_write_reg( CC2500_REG_PKTCTRL0,  cfg->pktctrl0  );
    }
        cc2500_device_context.settings.pktctrl0 = cfg->pktctrl0;
    if(cfg->addr != comp_cfg->addr){
        cc2500_spi_write_reg( CC2500_REG_ADDR,  cfg->addr  );
    }
        cc2500_device_context.settings.addr = cfg->addr;
    if(cfg->pktlen != comp_cfg->pktlen){
        cc2500_spi_write_reg( CC2500_REG_PKTLEN,  cfg->pktlen  );
    }
        cc2500_device_context.settings.pktlen = cfg->pktlen;
    cc2500_spi_write_reg( CC2500_PATABLE_ADDR, CC2500_PATABLE_VALUE );
}

void cc2500_configure(RF_SETTINGS const * cfg, int reset_state)
{
    cc2500_configure_hw(cfg, reset_state);

    // signal the state modification to the os
    drv_mark_dirty(&ctx_changes);
}


void cc2500_drv_save(int drv_handle)
{
    drv_save(&ctx_changes,
             syt_drv_get_ctx_next(drv_handle),
             &cc2500_device_context,
             sizeof(cc2500_device_context));
}

static void __attribute__((noinline)) cc2500_drv_restore_hw(void)
{
    cc2500_HW_init();

    cc2500_io_lines_init();

    /* /!\ cc2500_idle modifies cc2500_device_context.control_mode */
    cc2500_idle();

    // restore configuration
    cc2500_configure(&(cc2500_device_context.settings), 1);
}

void cc2500_drv_restore(int drv_handle, bool onboot)
{
    unsigned int mode_to_restore;

    if(!onboot && !drv_is_dirty(&ctx_changes))
    {
        // Nothing to restore, nothing to do, return
        return;
    }

    dma_memcpy(&cc2500_device_context,
               onboot ? syt_drv_get_ctx_last(drv_handle) : syt_drv_get_ctx_next(drv_handle),
               sizeof(cc2500_device_context));

    driver_registered = true;
    mode_to_restore = cc2500_device_context.control_mode;

    cc2500_drv_restore_hw();

    // restore hardware mode corresponding to driver data
    switch(mode_to_restore) {
        case CC2500_IDLE_DRV_MODE :
            // we already are in idle state
            break;
        case CC2500_SLEEP_DRV_MODE :
            cc2500_sleep();
            break;
        case CC2500_XOFF_DRV_MODE :
            // not implemented
            break;
        case CC2500_RX_DRV_MODE :
            cc2500_flush_rx();
            cc2500_rx_enter();
            break;
        default :
            syt_kernel_panic();
            break;
    }

    // mark as dirty to make sure that it will be saved to the new checkpoint
    // that is still uninitialized after driver restoration
    drv_mark_dirty(&ctx_changes);
}

void cc2500_get_stored_packet(struct cc2500_packet_t *dst)
{
    dma_memcpy(dst, &cc2500_device_context.packet, sizeof(*dst));
}


/******************************************************************/
/*********************************************************** ISR **/


#ifdef SYT_ES_RX
void cc2500_drv_on_interrupt(unsigned long event)
{
    if(!driver_registered)
        return;

    if(event == SYT_ES_RX)
        cc2500_rx_pkt_eop();
}
#endif

/**/
