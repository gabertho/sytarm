#ifndef IRQ_H
#define IRQ_H

#define enable_interrupts()  __asm__("cpsie i")
#define disable_interrupts() __asm__("cpsid i")

#endif

