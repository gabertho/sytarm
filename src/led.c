#include <stdbool.h>

#include "led.h"
#include "gpio.h"
#include "systimer.h"
#include "sytare/services.h"

static DRIVER struct led_t led;
static DONT_RESTORE struct led_t cache_led;
static DONT_RESTORE bool dirty;

void led_init(void)
{
    PIOD->wpmr = 0x50494f00; // disable PIOD write protection

    // pins: PD1~PD8
    PIOD->per = 0x1fe;
    PIOD->oer = 0x1fe;
    PIOD->pudr = 0x1fe;
    PIOD->ppddr = 0x1fe;
    PIOD->codr = 0x1fe;
}

void led_write_reverse(uint8_t x)
{
    uint8_t _x = x;

    uint32_t s = 0;
    for(size_t i = 1; i <= 8; ++i, _x >>= 1)
        s |= ((_x&1) << i);
    uint32_t c = ((~s) & 0x1fe);

    PIOD->sodr = s;
    PIOD->codr = c;

    // Save changes to cache first
    cache_led.displayed_number = x;
    dirty = true;
}

void led_long_nested_syscall(size_t wait_cycles)
{
    uint8_t x = 0;
    for(size_t i = 0; i < 8; ++i)
    {
        x |= (1u << (7 - i));
        led_write_reverse(x);
        systimer_delay_cycles(wait_cycles);
    }
    led_write_reverse(0);
}

void led_restore(void)
{
    led_init();
    led_write_reverse(led.displayed_number);

    cache_led = led;
    dirty = false;
}

void led_commit(void)
{
    if(!dirty)
        return;
    // Everything is done, save to device context
    led = cache_led;
    dirty = false;
}

