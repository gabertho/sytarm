#include "dacc.h"
#include "pmc.h"

#define DACC_PER_ID 30

void dacc_init(size_t channel)
{
    channel &= 1;
    uint32_t mask = (1 << channel);

    PMC->wpmr = 0x504d4300;
    PMC->pcer0 = (1 << DACC_PER_ID);
    PMC->pcr = (DACC_PER_ID)
             | (1 << 8)  // Main clock
             | (1 << 12) // CMD = write
             | (1 << 28) // Enable
             ;

    DACC->wpmr = 0x44414300;

    DACC->cr = 1;             // Software reset
    DACC->mr = 0              // MAXS0 (trigger mode or free-running mode)
             | (0 << 1)       // MAXS1 (trigger mode or free-running mode)
             | (0 << 4)       // Byte mode
             | (0 << 23)      // Single-ended outputs
             | (0 << 24)      // PRESCALER = 0 -> f_dac = f_peripheral / 2
             ;
    DACC->trigr = 0;          // Trigger mode disabled for both channels
    DACC->cher = mask;        // Enable corresponding channel
    DACC->idr = 3 | (3 << 4); // Disable all interrupts
    DACC->acr = 0;            // Bypass mode for both channels

    mask <<= 8;
    while(!(DACC->chsr & mask)); // Wait for DACRDYx to be 1

    DACC->wpmr = 0x44414301;
}

