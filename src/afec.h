#ifndef AFEC_H
#define AFEC_H

#include <stdint.h>
#include <stddef.h>

struct afec_t
{
    uint32_t
        cr,
        mr,
        emr,
        seq1r,
        seq2r,
        cher,
        chdr,
        chsr,
        lcdr,
        ier,
        idr,
        imr,
        isr,
        reserved0[6],
        over,
        cwr,
        cgr,
        reserved1,
        diffr,
        cselr,
        cdr,
        cocr,
        tempmr,
        tempcwr,
        reserved2[7],
        acr,
        reserved3[2],
        shmr,
        reserved4[11],
        cosr,
        cvr,
        cecr,
        reserved5[2],
        wpmr,
        wpsr
        ;
};

#define AFEC0 ((volatile struct afec_t*) 0x4003C000)
#define AFEC1 ((volatile struct afec_t*) 0x40064000)

void afec_enable_gpio(size_t index, size_t channel);

#endif

