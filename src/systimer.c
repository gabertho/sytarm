#include "systimer.h"

struct systimer_t
{
    uint32_t
        csr,
        rvr,
        cvr,
        calib;
};

#define SYSTIMER ((volatile struct systimer_t*) 0xe000e010)

#define CSR_OFF (1 << 2) // ENABLE=0, TICKINT=0, CLKSOURCE=processor clock
#define CSR_ON ((1 << 0) | (1 << 2)) // ENABLE=1, TICKINT=0, CLKSOURCE=processor clock

static size_t systimer_cycles;

void systimer_delay_cycles(size_t cycles)
{
    SYSTIMER->csr = CSR_OFF;
    SYSTIMER->rvr = cycles;
    SYSTIMER->csr = CSR_ON;

    systimer_cycles = cycles;

    while(!(SYSTIMER->csr & (1 << 16)));
    SYSTIMER->csr = CSR_OFF;

    systimer_cycles = 0;
}

void systimer_restore(void)
{
    SYSTIMER->csr = CSR_OFF;
    SYSTIMER->rvr = systimer_cycles;
    SYSTIMER->csr = CSR_ON;
}

