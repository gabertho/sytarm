#include "afec.h"
#include "gpio.h"

#define AFEC_BASE(index) (((index) == 1 ? AFEC1 : AFEC0))

#define ADC0AD0_MASK (1 << 30)
#define ADC0AD1_MASK (1 << 21)
#define ADC0AD2_MASK (1 << 3)

/*
 * 00: PD30
 * 01: PA21
 * 02: PB3
 */

void afec_enable_gpio(size_t index, size_t channel)
{
    volatile struct afec_t *base = AFEC_BASE(index);

    base->wpmr = 0x41444300;
    if(index == 1)
    {
        // TODO
    }
    else // 0
    {
        switch(channel)
        {
            case 1:
                PIOA->wpmr = 0x50494f00;
                PIOA->pdr = ADC0AD1_MASK;
                break;
            case 2:
                PIOB->wpmr = 0x50494f00;
                PIOB->pdr = ADC0AD2_MASK;
                break;
            default: // 0
                PIOD->wpmr = 0x50494f00;
                PIOD->pdr = ADC0AD0_MASK;
                break;
        }
    }
    base->cher = (1 << channel);
}

