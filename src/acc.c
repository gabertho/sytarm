#include "acc.h"
#include "pmc.h"
#include "afec.h"

#define ACC_PER_ID 33
#define WPMR_KEY 0x41434300

static inline void configure_pio(
    enum acc_plus_channel_e chan_plus,
    enum acc_minus_channel_e chan_minus
)
{
    if(chan_plus == ACC_P_CHAN_00 || chan_minus == ACC_M_CHAN_00)
        afec_enable_gpio(0, 0);
    if(chan_plus == ACC_P_CHAN_01 || chan_minus == ACC_M_CHAN_01)
        afec_enable_gpio(0, 1);
    if(chan_plus == ACC_P_CHAN_02 || chan_minus == ACC_M_CHAN_02)
        afec_enable_gpio(0, 2);
}

void acc_init(
    enum acc_plus_channel_e chan_plus,
    enum acc_minus_channel_e chan_minus
)
{
    configure_pio(chan_plus, chan_minus);

    PMC->wpmr = 0x504d4300;
    PMC->pcer1 = (1 << (ACC_PER_ID - 32));
    PMC->pcr = (ACC_PER_ID)
             | (1 << 8) // Main clock
             | (1 << 12) // CMD = write
             | (1 << 28) // Enable
             ;
    
    ACC->cr = 1; // ACC software reset
    ACC->wpmr = WPMR_KEY;

    ACC->acr = 0; // HYST = 0, ISEL = 0
    ACC->mr = chan_minus
            | (chan_plus << 4)
            | (1 << 8) // ACEN
            | (1 << 9) // EDGETYP = falling edge
            | (0 << 12) // INV
            | (0 << 13) // SELFS
            | (0 << 14) // FE
            ;
    ACC->ier = 1; // CE enable

    ACC->wpmr = WPMR_KEY | 1;
}

