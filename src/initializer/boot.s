.global _start
_start:
    ;@ Clear BSS
    ldr r0, =_sbss
    ldr r1, =_ebss
    mov r2, #0
bsszero:
    cmp r0, r1
    beq stackinit
    str r2, [r0]
    add r0, #4
    b bsszero

stackinit:
    ldr r0, =_estack
    mov sp, r0

    ;@ Squadala!
    b main

done:
    b done
