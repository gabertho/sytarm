#include <stddef.h>
#include <stdbool.h>

#include "watchdog.h"
#include "pmc.h"
#include "led.h"
#include "uart.h"
#include "nvram.h"
#include "irq.h"
#include "systimer.h"
#include "dma.h"

static void splash(void)
{
    for(size_t i = 3; i--; )
    {
        led_write_reverse(0xff);
        systimer_delay_cycles(1500000);
        led_write_reverse(0);
        systimer_delay_cycles(1500000);
    }
}

static bool check(const uint16_t *src, const uint16_t *dst, size_t size)
{
    for(size_t i = (size >> 1); i--;)
    {
        //uart0_print_h(*dst);
        if(*dst++ != *src++)
            return false;
    }

    return true;
}

static void __attribute__((noinline)) main_end(void)
{
    for(;;);
}

int main(void)
{
    extern const uint8_t nvram_contents[];
    extern const size_t sizeof_nvram_contents;

    extern const uint8_t checkpoint_contents[];
    extern const size_t sizeof_checkpoint_contents;
    extern void * const _scheckpoint0;

    watchdog_disable();
    disable_interrupts();
    pmc_set_main_and_master_clocks(MAIN_12MHZ, MASTER_PRES_1, MASTER_DIV_1);

    dma_init();
    led_init();
    uart0_init();
    nvram_init();
    systimer_delay_cycles(24000);

    // Slpashscreen
    splash();

    uart0_puts("Programming nvram...");
    dma_memcpy(NVRAM_MEM, (uint16_t*) nvram_contents, sizeof_nvram_contents);
    uart0_puts("Done!");
    uart0_puts("Checking nvram contents...");
    uart0_puts(check(NVRAM_MEM, (uint16_t*) nvram_contents, sizeof_nvram_contents) ? "Success!" : "Failed...");

    uart0_puts("Populating first checkpoint...");
    dma_memcpy(_scheckpoint0, (uint16_t*) checkpoint_contents, sizeof_checkpoint_contents);
    uart0_puts("Done!");
    uart0_puts("Checking checkpoint contents...");
    uart0_puts(check(_scheckpoint0, (uint16_t*) checkpoint_contents, sizeof_checkpoint_contents) ? "Success!" : "Failed...");

    splash();

    main_end();

    return 0;
}

