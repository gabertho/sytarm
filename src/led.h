#ifndef LED_H
#define LED_H

#include <stdint.h>
#include <stddef.h>

struct led_t
{
    uint8_t displayed_number;
};

void led_init(void);
void led_write_reverse(uint8_t x);

// Testing only
void led_long_nested_syscall(size_t wait_cycles);

// Sytare-specific
void led_restore(void);
void led_commit(void);

#endif

