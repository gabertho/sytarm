#ifndef SYSTEM_H
#define SYSTEM_H

#include <stdint.h>

struct system_t
{
    uint32_t
        cpuid,
        icsr,
        vtor,
        aircr,
        scr,
        ccr,
        shpr1,
        shpr2,
        shpr3,
        shcrs,
        cfsr,
        hfsr,
        reserved0,
        mmar,
        bfar,
        afsr
        ;
};

#define SYSTEM ((volatile struct system_t*) 0xe000ed00)

#define system_enable_memfault() (SYSTEM->shcrs |= (1u << 16)) // MEMFAULTENA

#endif

