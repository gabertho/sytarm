#ifndef WATCHDOG_H
#define WATCHDOG_H

#include <stdint.h>

struct watchdog_t
{
    uint32_t
        cr,
        mr,
        sr;
};

#define WDT ((volatile struct watchdog_t*) 0x400e1850)

#define watchdog_disable() (WDT->mr |= (1u << 15))

#endif

