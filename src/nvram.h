#ifndef NVRAM_H
#define NVRAM_H

#include "smc.h"
#include "gpio.h"

#define NVRAM_MEM ((uint16_t*) 0x60000000)
#define NVRAM_SIZE_BYTES 0x01000000

#define NVRAM __attribute__((section(".persistent")))
#define CNVRAM __attribute__((section(".persistent.const"))) const

void nvram_init();

#endif

