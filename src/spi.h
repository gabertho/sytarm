#ifndef SPI_H
#define SPI_H

#include <stdint.h>

#include "pmc.h"
#include "gpio.h"

struct spi_t
{
    uint32_t
        cr,
        mr,
        rdr,
        tdr,
        sr,
        ier,
        idr,
        imr,
        _pad0[4],
        csr[4],
        _pad1[41],
        wpmr,
        wpsr
        ;
};

#define SPI0 ((volatile struct spi_t*) 0x40008000)
#define SPI1 ((volatile struct spi_t*) 0x40058000)

void spi0_init();
void spi0_write_byte(uint8_t slave, uint8_t x);
uint8_t spi0_read_byte(uint8_t slave);

#endif

