#ifndef CC2500_H
#define CC2500_H

#include <stddef.h>

void cc2500_init();
void cc2500_send_packet(const void *buffer, size_t size);

#endif

