#ifndef DACC_H
#define DACC_H

#include <stdint.h>
#include <stddef.h>

struct dacc_t
{
    uint32_t
        cr,
        mr,
        trigr,
        reserved0,
        cher,
        chdr,
        chsr,
        cdr0,
        cdr1,
        ier,
        idr,
        imr,
        isr,
        reserved1[24],
        acr,
        reserved2[19],
        wpmr,
        wpsr
    ;
};

#define DACC ((volatile struct dacc_t*) 0x40040000)

void dacc_init(size_t channel);

#endif

