#ifndef UART_H
#define UART_H

#include <stdint.h>

#include "pmc.h"
#include "led.h"
#include "gpio.h"

struct uart_t
{
    uint32_t
        cr,
        mr,
        ier,
        idr,
        imr,
        sr,
        rhr,
        thr,
        brgr,
        cmpr,
        _pad0[47],
        wpmr;
};

#define UART0 ((volatile struct uart_t*) 0x400e0800)

void uart0_init();
void uart0_putc(char c);
void uart0_puts(const char *s);
void uart0_print_h(uint16_t x);
void uart0_print_u(uint32_t x);

#endif

