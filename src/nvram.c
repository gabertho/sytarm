#include "nvram.h"
#include "smc.h"
#include "pmc.h"
#include "gpio.h"

#define NVRAM_PORTA_MASK_C 0x001c0001
#define NVRAM_PORTA_MASK_A 0x00018000
#define NVRAM_PORTC_MASK 0xfffc49ff
#define NVRAM_PORTD_MASK 0x00008000
#define NVRAM_PORTE_MASK 0x0000003f

#define NVRAM_PORTD_ZZ_MASK (1 << 31)

#define SMC_PER_ID 9

void nvram_init()
{
    PIOA->wpmr     = 0x50494f00;
    PIOA->pdr      = (NVRAM_PORTA_MASK_A | NVRAM_PORTA_MASK_C);
    PIOA->abcdsr1 &= ~(NVRAM_PORTA_MASK_A | NVRAM_PORTA_MASK_C);
    PIOA->abcdsr2  = ((PIOA->abcdsr2 | NVRAM_PORTA_MASK_C) & ~NVRAM_PORTA_MASK_A);

    PIOC->wpmr     = 0x50494f00;
    PIOC->pdr      = NVRAM_PORTC_MASK;
    PIOC->abcdsr1 &= ~NVRAM_PORTC_MASK;
    PIOC->abcdsr2 &= ~NVRAM_PORTC_MASK;

    PIOD->wpmr     = 0x50494f00;
    PIOD->per      = NVRAM_PORTD_ZZ_MASK;
    PIOD->oer      = NVRAM_PORTD_ZZ_MASK;
    PIOD->sodr     = NVRAM_PORTD_ZZ_MASK;
    PIOD->pdr      = NVRAM_PORTD_MASK;
    PIOD->abcdsr1 &= ~NVRAM_PORTD_MASK;
    PIOD->abcdsr2 |= NVRAM_PORTD_MASK;

    PIOE->wpmr     = 0x50494f00;
    PIOE->pdr      = NVRAM_PORTE_MASK;
    PIOE->abcdsr1 &= ~NVRAM_PORTE_MASK;
    PIOE->abcdsr2 &= ~NVRAM_PORTE_MASK;

    PMC->wpmr = 0x504d4300;
    PMC->pcer0 = (1 << SMC_PER_ID);
    PMC->pcr = SMC_PER_ID
             | (1 << 8) // Main clock
             | (1 << 12) // CMD = write
             | (1 << 28) // Enable
             ;

    SMC->wpmr = 0x534d4300;
    SMC->ocms = 0x00000000;
    SMC->chans[0].mode = 1 // READ ocontrolled by NRD
                       | (1 << 1) // WRITE controlled by NWE
                       | (0 << 4) // No NWAIT signal
                       | (0 << 8) // BYTE_SELECT
                       | (1 << 12) // 16-bit data bus
                       | (15 << 16) // TDF cycles
                       | (0 << 20) // No TDF optimization
                       | (0 << 24) // Page mode disabled
                       ;
    SMC->chans[0].setup = 0x02020202; // 2 cycles @12MHz = 167ns
    SMC->chans[0].pulse = 0x02020202; // 2 cycles @12MHz = 167ns
    SMC->chans[0].cycle = 0x00050005;
}

