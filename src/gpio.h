#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>

struct pio_t
{
    uint32_t
        per,
        pdr,
        psr,
        _pad0,
        oer,
        odr,
        osr,
        _pad1,
        ifer,
        ifdr,
        ifsr,
        _pad2,
        sodr,
        codr,
        odsr,
        pdsr,
        ier,
        idr,
        imr,
        isr,
        mder,
        mddr,
        mdsr,
        _pad3,
        pudr,
        puer,
        pusr,
        _pad4,
        abcdsr1,
        abcdsr2,
        _pad5[2],
        ifscdr,
        ifscer,
        ifscsr,
        scdr,
        ppddr,
        ppder,
        ppdsr,
        _pad6,
        ower,
        owdr,
        owsr,
        _pad7,
        aimer,
        aimdr,
        aimmr,
        _pad8,
        esr,
        lsr,
        elsr,
        _pad9,
        fellsr,
        rehlsr,
        frlhsr,
        _pada,
        locksr,
        wpmr,
        wpsr
        ; /* TODO: rest */
};

#define PIOA ((volatile struct pio_t*) 0x400e0e00)
#define PIOB ((volatile struct pio_t*) 0x400e1000)
#define PIOC ((volatile struct pio_t*) 0x400e1200)
#define PIOD ((volatile struct pio_t*) 0x400e1400)
#define PIOE ((volatile struct pio_t*) 0x400e1600)

#endif

