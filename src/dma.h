#ifndef DMA_H
#define DMA_H

#include <stdint.h>
#include <stddef.h>

void dma_init(void);
void dma_memcpy(void *dst, const void *src, size_t size);
void dma_fast_memcpy32(void *dst, const void *src, size_t size);

#endif

