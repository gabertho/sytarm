#include <stdint.h>
#include <stddef.h>

#include "uart.h"
#include "led.h"
#include "sytare/services.h"

#define _2LOG2_LENGTH 14
#define LENGTH (1 << (_2LOG2_LENGTH))

static uint16_t array[LENGTH];

static void init_array(uint16_t *array)
{
    uint16_t tmp = 0xdead;
    for(size_t i = LENGTH; i--;)
    {
        //uart0_print_u(i);
        array[i] = tmp;
        tmp = 0xca * tmp + 0xde;
    }
}

static void swap(uint16_t *array, size_t i, size_t j)
{
    uint16_t tmp = array[i];
    array[i] = array[j];
    array[j] = tmp;
}

// Assumption: end >= start + 2
static size_t partition(uint16_t *array, size_t p, size_t start, size_t end)
{
    size_t l = start;
    size_t h = end - 2;
    uint16_t pivot = array[p];
    swap(array, p, end - 1);

    while(l < h)
    {
        if(array[l] < pivot)
            ++l;
        else if(array[h] >= pivot)
            --h;
        else
            swap(array, l, h);
    }

    size_t index = h + (array[h] < pivot);
    swap(array, end-1, index);
    return index;
}

static void quicksort(uint16_t *array, size_t length)
{
    static size_t starts[_2LOG2_LENGTH];
    static size_t ends[_2LOG2_LENGTH];
    size_t lstarts = 1;
    size_t lends = 1;

    size_t beacon = 0;

    starts[0] = 0;
    ends[0] = length;

    while(lstarts)
    {
        size_t start = starts[--lstarts];
        size_t end = ends[--lends];

        if(end >= start + 2)
        {
            size_t p = (start + end) / 2;
            p = partition(array, p, start, end);

            starts[lstarts++] = p+1;
            starts[lstarts++] = start;

            ends[lends++] = end;
            ends[lends++] = p;
        }

        SYT_BEGIN_ATOMIC();
        led_write_reverse(++beacon);
        SYT_END_ATOMIC();
    }
}

void application_main(void)
{
    SYT_BEGIN_ATOMIC();
    uart0_puts("Init array...");
    SYT_END_ATOMIC();

    init_array(array);

    SYT_BEGIN_ATOMIC();
    uart0_puts("Ok!");
    SYT_END_ATOMIC();

    quicksort(array, LENGTH);

    for(;;);
}

