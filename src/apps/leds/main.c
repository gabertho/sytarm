#include <stddef.h>

#include "led.h"
#include "systimer.h"

#include "sytare/services.h"

void application_main(void)
{
    // Actual application start
    uint8_t i = 1;
    for(size_t j = 1; ; ++j)
    {
        if(j == 4)
        {
            SYT_BEGIN_ATOMIC();
            /* 1600000: Fails during the first lifecycle, but runs to completion
             * in the second lifecycle. This shows that atomic sections are
             * rerun from the beginning instead of resumed at the middle.
             *
             * 3000000: Always too long to fit in a single lifecycle. The
             * application will be stuck in a loop, trying to run this atomic
             * section, rebooting, trying again, and so on.
             */
            led_long_nested_syscall(1600000);
            SYT_END_ATOMIC();
        }

        SYT_BEGIN_ATOMIC();
        led_write_reverse(i);
        SYT_END_ATOMIC();

        systimer_delay_cycles(3000000);

        if(j == 16)
        {
            i = 1;
            j = 0;
        }
        else if(!(j & 1))
            i &= ~(1u << ((j>>1)-1));
        else
            i |= (1u << ((j>>1)+1));
    }
}

