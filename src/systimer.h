#ifndef SYSTIMER_H
#define SYSTIMER_H

#include <stdint.h>
#include <stddef.h>

void systimer_delay_cycles(size_t cycles);

// Sytare-specific
void systimer_restore(void);

#endif

