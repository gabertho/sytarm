#ifndef ACC_H
#define ACC_H

#include <stdint.h>

struct acc_t
{
    uint32_t
        cr,
        mr,
        reserved0[7],
        ier,
        idr,
        imr,
        isr,
        reserved1[24],
        acr,
        reserved2[19],
        wpmr,
        wpsr
        ;
};

#define ACC ((volatile struct acc_t*) 0x40044000)

enum acc_plus_channel_e
{
    ACC_P_CHAN_00 = 0, // AFE0_AD0
    ACC_P_CHAN_01 = 1, // AFE0_AD1
    ACC_P_CHAN_02 = 2  // AFE0_AD2
};

enum acc_minus_channel_e
{
    ACC_M_CHAN_TS    = 0, // Temperature sensor
    ACC_M_CHAN_VREFP = 1,
    ACC_M_CHAN_DAC0  = 2,
    ACC_M_CHAN_DAC1  = 3,
    ACC_M_CHAN_00    = 4, // AFE0_AD0
    ACC_M_CHAN_01    = 5, // AFE0_AD1
    ACC_M_CHAN_02    = 6  // AFE0_AD2
};

void acc_init(
    enum acc_plus_channel_e chan_plus,
    enum acc_minus_channel_e chan_minus
);

#endif

