#include "dma.h"
#include "pmc.h"

#define DMA_PER_ID 58

// Channel 0 is reserved for fast memory-to-memory 32bit copies

struct dma_channel_t
{
    uint32_t
        ie,
        id,
        im,
        is,
        sa,
        da,
        nda,
        ndc,
        ubc,
        bc,
        c,
        ds_msp,
        sus,
        dus,
        reserved[2]
        ;
};

struct dma_t
{
    uint32_t
        gtype,
        gcfg,
        gwac,
        gie,
        gid,
        gim,
        gis,
        ge,
        gd,
        gs,
        grs,
        gws,
        grws,
        grwr,
        gswr,
        gsws,
        gswf,
        reserved[3]
        ;
    struct dma_channel_t channels[24];
};

#define DMA ((volatile struct dma_t*) 0x40078000)

void dma_init(void)
{
    PMC->wpmr = 0x504D4300;
    PMC->pcer1 = (1 << (DMA_PER_ID - 32));
    PMC->pcr  = (DMA_PER_ID)
              | (1 << 8)  // Main clock (4 for master clock)
              | (1 << 12) // CMD = write
              | (1 << 28) // Enable
              ;
}

static inline size_t allocate_channel(void)
{
    // Remember that channel 0 is reserved
    const uint32_t mask = 0x00fffffe;
    uint32_t gs = DMA->gs;
    while((gs & mask) == mask) // Wait for a channel to be disabled
        gs = DMA->gs;
    
    size_t i = 0;
    gs >>= 1; // Channel 0 is reserved, skip it
    for(; (gs & 1); ++i, gs >>= 1);
    
    // Return the first available channel
    return i;
}

void dma_memcpy(void *dst, const void *src, size_t size)
{
    // 1. Get channel x
    size_t _channel = allocate_channel();
    volatile struct dma_channel_t *channel = &DMA->channels[_channel];

    // 2. Clear ISx by reading it
    volatile uint32_t is = channel->is;
    (void) is;

    // 3. Set source address
    channel->sa = (uint32_t) src;

    // 4.Set destination address
    channel->da = (uint32_t) dst;

    // 5. Set transfer length
    channel->ubc = size; // TODO: check if bytes or words

    // 6. Set Cx
    channel->c = (0 << 0) // TYPE = memory-to-memory
               | (0 << 1) // MBSIZE = SINGLE burst
               | (0 << 7) // MEMSET = 0
               | (0 << 11) // DWIDTH = byte transfer
               | (1 << 16) // SAM = increment source address
               | (1 << 18) // DAM = increment destination address
               ;

    // 7. Disable all blocks since we perform a single-block transfer
    channel->ndc = 0;
    channel->bc = 0;
    channel->ds_msp = 0;
    channel->sus = 0;
    channel->dus = 0;

    // 8. Enable channel
    DMA->ge = (1 << _channel);

    // 9. Wait until transfer is done
    while(!(channel->is & 1));
}

void dma_fast_memcpy32(void *dst, const void *src, size_t size)
{
    // Channel 0 only
    // Assumes that it is only used for memory-to-memory

    // 1. Get channel x
    volatile struct dma_channel_t *channel = &DMA->channels[0];

    // 3. Set source address
    channel->sa = (uint32_t) src;

    // 4.Set destination address
    channel->da = (uint32_t) dst;

    // 5. Set transfer length
    channel->ubc = (size >> 2);

    // 6. Set Cx
    channel->c = (0 << 0) // TYPE = memory-to-memory
               | (0 << 1) // MBSIZE = SINGLE burst
               | (0 << 7) // MEMSET = 0
               | (2 << 11) // DWIDTH = 32-bit transfer
               | (1 << 16) // SAM = increment source address
               | (1 << 18) // DAM = increment destination address
               ;

    // 8. Enable channel
    __asm__("dsb");
    DMA->ge = (1 << 0); // Channel 0

    // 9. Wait until transfer is done
    while(!(channel->is & 1));
}

