#ifndef NVIC_H
#define NVIC_H

struct nvic_t
{
    uint32_t
        iser[8],
        reserved0[24],
        icer[8],
        reserved1[24],
        ispr[8],
        reserved2[24],
        icpr[8],
        reserved3[24],
        iabr[8],
        reserved4[56],
        ipr[60],
        reserved5[644],
        stir
        ;
};

#define NVIC ((volatile struct nvic_t*) 0xe000e100)

#define nvic_enable(id)  (NVIC->iser[(id) >> 5] = (1u << ((id) & 0x1f)))
#define nvic_disable(id) (NVIC->icer[(id) >> 5] = (1u << ((id) & 0x1f)))
#define nvic_clear(id)   (NVIC->icpr[(id) >> 5] = (1u << ((id) & 0x1f)))

#endif

