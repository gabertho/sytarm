#ifndef RTT_H
#define RTT_H

#include <stdint.h>

struct rtt_t
{
    uint32_t
        mr,
        ar,
        vr,
        sr;
};

#define RTT ((volatile struct rtt_t*) 0x400e1830)
#define RTT_PER_ID 3

// f_RTT < 42 kHz
#define RTT_AT_LEAST_US(us) ((us) * 21 / 500)

// f_RTT > 22 kHz
#define RTT_AT_MOST_US(us) ((us) * 11 / 500)

void rtt_set_periodic(uint16_t cycles);
void rtt_ack_interrupt(void);

// Sytare-specific
void rtt_restore(void);

#endif

