import sys

BASE_ADDRESS  = 0x400000
MAIN_SP_VALUE = ".word _estack"

N_IRQ = 16 + 71
NAMES = [
    "main_sp",
    "reset",
    "nmi",
    "hard_fault",
    "mem_fault",
    "bus_fault",
    "usage_fault",
    "reserved0",
    "reserved1",
    "reserved2",
    "reserved3",
    "svc",
    "reserved4",
    "reserved5",
    "pendsv",
    "systick"
]
HANDLERS = {
    3 : "syt_checkpoint_handler",
    "svc_handler": "syt_svc_handler"
}

if len(sys.argv) > 1:
    args = list(i.lower() for i in sys.argv[1].split(","))
    for arg in args:
        if arg == "use_mpu":
            HANDLERS["mem_fault_handler"] = "syt_mpu_fault_handler"

def irq2label(irq):
    if irq < 16:
        label = NAMES[irq]
    else:
        label = ("irq%02d"  % (irq-16))
    if irq > 0:
        label += "_handler"
    return label

def irq2address(irq):
    return BASE_ADDRESS + irq*4

print(".section .vectors")
print()

for irq in range(N_IRQ):
    label = irq2label(irq)
    if irq == 0:
        print(".word _estack")
    elif irq == 1:
        print(".word _start+1")
    elif label in HANDLERS.keys():
        print("%s:\n    .word %s + 1\n"%(label, HANDLERS[label]))
    elif irq-16 in HANDLERS.keys():
        print("%s:\n    .word %s + 1\n"%(label, HANDLERS[irq-16]))
    else:
        print("%s:\n    .word 0x%08x\n"%(label, irq2address(irq)|1))

