import sys

with open(sys.argv[1], "rb") as f:
    contents = f.read()
    byte_sequence = list(int(i) for i in contents)

    print("#include <stdint.h>")
    print("#include <stddef.h>")
    print("const uint8_t nvram_contents[%d] = {" % len(byte_sequence))
    print(",".join("0x%02x" % b for b in byte_sequence))
    print("};")
    print("const size_t sizeof_nvram_contents = %d;" % len(byte_sequence))

