import sys
import subprocess

def address_of_symbol(kernel, symbol):
    # arm-none-eabi-readelf -s kernel | awk '$8 == "symbol" {print $2}'
    process = subprocess.Popen(["arm-none-eabi-readelf", "-s", kernel], stdout=subprocess.PIPE)
    output = subprocess.check_output(["awk", "$8 == \"%s\" {print $2}" % symbol], stdin = process.stdout).decode(sys.stdout.encoding)
    process.wait()
    return int(output, 16)
