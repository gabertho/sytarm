import sys

import elfutils

def create_checkpoint(application_main):
    stack = [
        0xfffffff9, # exc_return
        0x0, # r4-r11
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0,
        0x0, # r0-r3
        0x0,
        0x0,
        0x0,
        0x0, # r12
        0x0, # lr
        application_main, # pc
        0x01000000 # xpsr
    ]
    return stack

def data2bytes_little_endian(data):
    ret = list()
    for d in data:
        ret.extend([d & 0xff, (d>>8)&0xff, (d>>16)&0xff, d>>24])
    return ret

application_main = elfutils.address_of_symbol(sys.argv[1], "application_main")
_scheckpoint0 = elfutils.address_of_symbol(sys.argv[1], "_scheckpoint0")

checkpoint = data2bytes_little_endian(create_checkpoint(application_main))

print("#include <stdint.h>")
print("#include <stddef.h>")
print("const uint8_t checkpoint_contents[%d] = {" % len(checkpoint))
print(",".join("0x%02x" % b for b in checkpoint))
print("};")
print("const size_t sizeof_checkpoint_contents = %d;" % len(checkpoint))
print("void * const _scheckpoint0 = (void*) 0x%08x;" % _scheckpoint0)

