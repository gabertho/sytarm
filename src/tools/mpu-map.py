import sys
import math

import elfutils

N_REGIONS = 16
ALIGNMENT = 1024 # Manually synchronized with `src/ldscript.ld`
MIN = 32
BASE_ADDRESS = 0x20400000

ACCESS_RIGHTS = {
    "XN"  : 1,
    "AP"  : 7,
    "TEX" : 0,
    "S"   : 0,
    "C"   : 0,
    "B"   : 0,
    "SRD" : 0
}

PROTOTYPE_INIT     = "void mpu_hw_init(void)"
PROTOTYPE_REGIONOF = "size_t mpu_region_of(uint32_t address)"
PROTOTYPE_COPY     = "void mpu_copy_region(uint32_t *base_address, size_t region)"

_sdata = elfutils.address_of_symbol(sys.argv[1], "_sdata")
_edata = elfutils.address_of_symbol(sys.argv[1], "_edata")
_sbss = elfutils.address_of_symbol(sys.argv[1], "_sbss")
_ebss = elfutils.address_of_symbol(sys.argv[1], "_ebss")
total_size = _edata - _sdata + _ebss - _sbss

def prev_power_of_2(x):
    return 1 << math.floor(math.log2(x))

def next_power_of_2(x):
    return 1 << math.ceil(math.log2(x))

def clip_to_min(x):
    if x < MIN:
        return MIN
    return x

def multi_gcd(t):
    if len(t) == 1:
        return t[0]
    return math.gcd(t[0], multi_gcd(t[1:]))

def addresses(regions):
    address = BASE_ADDRESS
    t = list()
    for region in regions:
        address += region
        t.append(address)
    return t

def find_prefix_mask(t):
    mask = 0xffffffff
    while (mask & 0xffffffff) != 0:
        u = set(i & mask for i in t)
        if len(u) == 1:
            return 0xffffffff - (mask & 0xffffffff)
        mask <<= 1
    return 0xffffffff

def mpu_map(base_address, alignment, contents_size, remaining_regions, regions):
    if contents_size <= 0 or remaining_regions <= 0:
        return regions

    if remaining_regions == N_REGIONS or remaining_regions == 1:
        size = min(alignment, max(MIN, contents_size))
        region_size = next_power_of_2(size)
    elif contents_size < alignment:
        region_size = clip_to_min(next_power_of_2(contents_size))
    else:
        region_size = clip_to_min(next_power_of_2(contents_size / remaining_regions))
        while (base_address & (region_size - 1)) != 0:
            region_size >>= 1

    return mpu_map(base_address + region_size, alignment, contents_size - region_size, remaining_regions - 1, regions + [region_size])

def regions_to_c(regions):
    print("#include <stdint.h>")
    print('#include "sytare/mpu.h"')
    print('#include "dma.h"')

    if len(regions) > 1:
        print("const unsigned int disable_mpu = 0;")
    else:
        print("const unsigned int disable_mpu = 1;")

    print(PROTOTYPE_INIT)
    print("{")

    if len(regions) > 1: # One region <=> no need for mpu
        address = BASE_ADDRESS
        print("    const uint32_t access_rights = MPU_ACCESS_RIGHTS(%d, %d, %d, %d, %d, %d, %d);" % (ACCESS_RIGHTS["XN"], ACCESS_RIGHTS["AP"], ACCESS_RIGHTS["TEX"], ACCESS_RIGHTS["S"], ACCESS_RIGHTS["C"], ACCESS_RIGHTS["B"], ACCESS_RIGHTS["SRD"]))
        for i, region in enumerate(regions):
            mask = ~(region - 1)
            size_log2_minus_1 = math.floor(math.log2(region)) - 1
            print()
            print("    MPU->rbar = %d | (1 << 4) | 0x%08xu;" % (i, address & mask))
            print("    MPU->rasr = (1 << 0) | (%d << 1) | access_rights;" % size_log2_minus_1)

            address += region
    print("}\n")

    print(PROTOTYPE_REGIONOF)
    print("{")
    if len(regions) > 1:
        address = BASE_ADDRESS
        gcd = multi_gcd(regions)
        prefix_mask = find_prefix_mask(list(i // gcd for i in addresses(regions)))
        print("    address /= %d;" % gcd)
        print("    address &= 0x%08x;" % prefix_mask)
        for i, region in enumerate(regions[:-1]):
            address += region
            print("    if(address < 0x%08x)" % ((address // gcd) & prefix_mask))
            print("        return %d;" % i)
        print("    return %d;" % (len(regions)-1))
    else:
        print("    (void) address;")
        print("    return 0;")
    print("}\n")

    print(PROTOTYPE_COPY)
    print("{")
    print("    const void *src;")
    print("    void *dst;")
    print("    size_t size;")
    print("    switch(region)")
    print("    {")
    accumulated = regions[0]
    for _i, region in enumerate(regions[1:]):
        i = _i + 1
        print("        case %d:" % i)
        print("            src = (void*) 0x%08x;" % (BASE_ADDRESS + accumulated))
        print("            dst = base_address + 0x%08x;" % (accumulated >> 2))
        print("            size = %d;" % region)
        print("            break;")
        accumulated += region
    print("        default:")
    print("            src = (void*) 0x%08x;" % BASE_ADDRESS)
    print("            dst = base_address;")
    print("            size = %d;" % regions[0])
    print("    }")
    print("    dma_fast_memcpy32(dst, src, size);")
    print("}\n")

regions = mpu_map(BASE_ADDRESS, ALIGNMENT, total_size, N_REGIONS, list())
regions_to_c(regions)
