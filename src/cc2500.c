#include "cc2500.h"
#include "spi.h"

/* C2500 Registers: table 36, page 60 */
#define CC2500_REG_IOCFG2                       0x00
#define CC2500_REG_IOCFG1                       0x01
#define CC2500_REG_IOCFG0                       0x02
#define CC2500_REG_FIFOTHR                      0x03
#define CC2500_REG_SYNC1                        0x04
#define CC2500_REG_SYNC0                        0x05
#define CC2500_REG_PKTLEN                       0x06
#define CC2500_REG_PKTCTRL1                     0x07
#define CC2500_REG_PKTCTRL0                     0x08
#define CC2500_REG_ADDR                         0x09
#define CC2500_REG_CHANNR                       0x0A
#define CC2500_REG_FSCTRL1                      0x0B
#define CC2500_REG_FSCTRL0                      0x0C
#define CC2500_REG_FREQ2                        0x0D
#define CC2500_REG_FREQ1                        0x0E
#define CC2500_REG_FREQ0                        0x0F
#define CC2500_REG_MDMCFG4                      0x10
#define CC2500_REG_MDMCFG3                      0x11
#define CC2500_REG_MDMCFG2                      0x12
#define CC2500_REG_MDMCFG1                      0x13
#define CC2500_REG_MDMCFG0                      0x14
#define CC2500_REG_DEVIATN                      0x15
#define CC2500_REG_MCSM2                        0x16
#define CC2500_REG_MCSM1                        0x17
#define CC2500_REG_MCSM0                        0x18
#define CC2500_REG_FOCCFG                       0x19
#define CC2500_REG_BSCFG                        0x1A
#define CC2500_REG_AGCCTRL2                     0x1B
#define CC2500_REG_AGCCTRL1                     0x1C
#define CC2500_REG_AGCCTRL0                     0x1D
#define CC2500_REG_WOREVT1                      0x1E
#define CC2500_REG_WOREVT0                      0x1F
#define CC2500_REG_WORCTRL                      0x20
#define CC2500_REG_FREND1                       0x21
#define CC2500_REG_FREND0                       0x22
#define CC2500_REG_FSCAL3                       0x23
#define CC2500_REG_FSCAL2                       0x24
#define CC2500_REG_FSCAL1                       0x25
#define CC2500_REG_FSCAL0                       0x26
#define CC2500_REG_RCCTRL1                      0x27
#define CC2500_REG_RCCTRL0                      0x28
/* Test registers */
#define CC2500_REG_FSTEST                       0x29
#define CC2500_REG_PTEST                        0x2A
#define CC2500_REG_AGCTEST                      0x2B
#define CC2500_REG_TEST2                        0x2C
#define CC2500_REG_TEST1                        0x2D
#define CC2500_REG_TEST0                        0x2E
/* Read only registers, access with |0xc0 */
#define CC2500_REG_PARTNUM                      0x30 /* 0xF0 */
#define CC2500_REG_VERSION                      0x31 /* 0xF1 */
#define CC2500_REG_FREQEST                      0x32
#define CC2500_REG_LQI                          0x33
#define CC2500_REG_RSSI                         0x34
#define CC2500_REG_MARCSTATE                    0x35
#define CC2500_REG_WORTIME1                     0x36
#define CC2500_REG_WORTIME0                     0x37
#define CC2500_REG_PKTSTATUS                    0x38
#define CC2500_REG_VCO_VC_DAC                   0x39
#define CC2500_REG_TXBYTES                      0x3A
#define CC2500_REG_RXBYTES                      0x3B
#define CC2500_REG_RCCTRL1_STATUS               0x3C
#define CC2500_REG_RCCTRL0_STATUS               0x3D
#define CC2500_PATABLE_ADDR                     0x3E
#define CC2500_DATA_FIFO_ADDR                   0x3F

/* CC2500 RAM & register Access (table 37, 61) */
#define CC2500_REG_ACCESS_OP(x)                 (x & 0x80)
#define CC2500_REG_ACCESS_OP_READ               0x80
#define CC2500_REG_ACCESS_OP_WRITE              0x00
#define CC2500_REG_ACCESS_TYPE(x)               (x & 0x40)
#define CC2500_REG_WRITE_BURST                  (0x40)
#define CC2500_REG_READ_BURST                   (0xc0) /* cf. p. 60 datasheet */
#define CC2500_REG_ACCESS_NOBURST               0x00
#define CC2500_REG_ACCESS_ADDRESS(x)            (x & 0x3F)

/* STROBE: table 34, page 58 */
#define CC2500_STROBE_SRES                      0x30 /* reset                                       */
#define CC2500_STROBE_SFSTXON                   0x31 /* enable and calibrate                        */
#define CC2500_STROBE_SXOFF                     0x32 /* crystall off                                */
#define CC2500_STROBE_SCAL                      0x33 /* calibrate                                   */
#define CC2500_STROBE_SRX                       0x34 /* enable rx                                   */
#define CC2500_STROBE_STX                       0x35 /* enable tx                                   */
#define CC2500_STROBE_SIDLE                     0x36 /* go idle                                     */
#define CC2500_STROBE_SWOR                      0x38 /* wake on radio                               */
#define CC2500_STROBE_SPWD                      0x39 /* power down                                  */
#define CC2500_STROBE_SFRX                      0x3A /* flush Rx fifo                               */
#define CC2500_STROBE_SFTX                      0x3B /* flush Tx fifo                               */
#define CC2500_STROBE_SWORRST                   0x3C /* Reset real time clock to Event1 value.      */
#define CC2500_STROBE_SNOP                      0x3D /* no operation                                */

/* Driver defines */
#define CC2500_GDOx_CHIP_RDY                    0x29 /* CHIP_RDY     */
#define CC2500_GDOx_XOSC_STABLE                 0x2B /* XOSC_STABLE  */
#define CC2500_RADIO_PARTNUM                    0x80
#define CC2500_SETTING_PATABLE0                 0xFE
#define CC2500_TEST_VALUE                       0xA5
#define CC2500_FRAME_RSSI_OFFSET                0
#define CC2500_FRAME_LQI_OFFSET                 1
#define CC2500_RSSI_OFFSET                      72
#define CC2500_FIFO_THRESHOLD                   15
#define CC2500_PATABLE_VALUE                    0xFE

#define SPI_SLAVE_ID 0

struct RF_SETTINGS_t {
    uint8_t fsctrl1;       // Frequency synthesizer control.
    uint8_t fsctrl0;       // Frequency synthesizer control.
    uint8_t freq2;         // Frequency control word, high byte.
    uint8_t freq1;         // Frequency control word, middle byte.
    uint8_t freq0;         // Frequency control word, low byte.
    uint8_t mdmsettings4;       // Modem configuration.
    uint8_t mdmsettings3;       // Modem configuration.
    uint8_t mdmsettings2;       // Modem configuration.
    uint8_t mdmsettings1;       // Modem configuration.
    uint8_t mdmsettings0;       // Modem configuration.
    uint8_t channr;        // Channel number.
    uint8_t deviatn;       // Modem deviation setting (when FSK modulation is enabled).
    uint8_t frend1;        // Front end RX configuration.
    uint8_t frend0;        // Front end TX configuration.
    uint8_t mcsm0;         // Main Radio Control State Machine configuration.
    uint8_t focsettings;        // Frequency Offset Compensation Configuration.
    uint8_t bssettings;         // Bit synchronization Configuration.
    uint8_t agcctrl2;      // AGC control.
    uint8_t agcctrl1;      // AGC control.
    uint8_t agcctrl0;      // AGC control.
    uint8_t fscal3;        // Frequency synthesizer calibration.
    uint8_t fscal2;        // Frequency synthesizer calibration.
    uint8_t fscal1;        // Frequency synthesizer calibration.
    uint8_t fscal0;        // Frequency synthesizer calibration.
    uint8_t fstest;        // Frequency synthesizer calibration.
    uint8_t test2;         // Various test settings.
    uint8_t test1;         // Various test settings.
    uint8_t test0;         // Various test settings.
    uint8_t fifothr;       // RXFIFO and TXFIFO thresholds.
    uint8_t iosettings2;        // GDO2 output pin configuration.
    uint8_t iosettings0d;       // GDO0 output pin configuration. Refer to SmartRF Studio User Manual
                        // for detailed pseudo register explanation.
    uint8_t pktctrl1;      // Packet automation control.
    uint8_t pktctrl0;      // Packet automation control.
    uint8_t addr;          // Device address.
    uint8_t pktlen;        // Packet length.
};

static const struct RF_SETTINGS_t default_settings = {
    0x12,   // FSCTRL1   Frequency synthesizer control.
    0x00,   // FSCTRL0   Frequency synthesizer control.
    0x5D,   // FREQ2     Frequency control word, high byte.
    0x93,   // FREQ1     Frequency control word, middle byte.
    0xB1,   // FREQ0     Frequency control word, low byte.
    0x2D,   // MDMCFG4   Modem configuration.
    0x3B,   // MDMCFG3   Modem configuration.
    0xF3,   // MDMCFG2   Modem configuration.
    0x22,   // MDMCFG1   Modem configuration.
    0xF8,   // MDMCFG0   Modem configuration.
    0x00,   // CHANNR    Channel number.
    0x01,   // DEVIATN   Modem deviation setting (when FSK modulation is enabled).
    0xB6,   // FREND1    Front end RX configuration.
    0x10,   // FREND0    Front end TX configuration.
    0x18,   // MCSM0     Main Radio Control State Machine configuration.
    0x1D,   // FOCCFG    Frequency Offset Compensation Configuration.
    0x1C,   // BSCFG     Bit synchronization Configuration.
    0xC7,   // AGCCTRL2  AGC control.
    0x00,   // AGCCTRL1  AGC control.
    0xB0,   // AGCCTRL0  AGC control.
    0xEA,   // FSCAL3    Frequency synthesizer calibration.
    0x0A,   // FSCAL2    Frequency synthesizer calibration.
    0x00,   // FSCAL1    Frequency synthesizer calibration.
    0x11,   // FSCAL0    Frequency synthesizer calibration.
    0x59,   // FSTEST    Frequency synthesizer calibration.
    0x88,   // TEST2     Various test settings.
    0x31,   // TEST1     Various test settings.
    0x0B,   // TEST0     Various test settings.
    0x07,   // FIFOTHR   RXFIFO and TXFIFO thresholds.
    0x29,   // IOCFG2    GDO2 output pin configuration.
    0x06,   // IOCFG0D   GDO0 output pin configuration. Refer to SmartRF® Studio User Manual for detailed pseudo register explanation.
    0x08,   // PKTCTRL1  Packet automation control.
    0x45,   // PKTCTRL0  Packet automation control.
    0x00,   // ADDR      Device address.
    0xFF    // PKTLEN    Packet length.
};

#define cc2500_spi_enable()
#define cc2500_spi_disable()

static void cc2500_spi_write_reg(uint8_t reg_adr, uint8_t reg_val)
{
    cc2500_spi_enable();
    spi0_write_byte(SPI_SLAVE_ID, reg_adr | CC2500_REG_ACCESS_OP_WRITE | CC2500_REG_ACCESS_NOBURST);
    spi0_write_byte(SPI_SLAVE_ID, reg_val);
    cc2500_spi_disable();
}

static void cc2500_spi_tx_fifo_byte(uint8_t tx_fifo_byte)
{
    cc2500_spi_enable();
    spi0_write_byte(SPI_SLAVE_ID, CC2500_DATA_FIFO_ADDR);
    spi0_write_byte(SPI_SLAVE_ID, tx_fifo_byte);
    cc2500_spi_disable();
}

static void cc2500_spi_tx_fifo_burst(const void *val, size_t len)
{
    const uint8_t *ptr = val;
    cc2500_spi_enable();
    spi0_write_byte(SPI_SLAVE_ID, CC2500_DATA_FIFO_ADDR | CC2500_REG_WRITE_BURST);
    while(len--)
        spi0_write_byte(SPI_SLAVE_ID, *ptr++);
    cc2500_spi_disable();
}

static uint8_t cc2500_spi_read_ro_reg(uint8_t reg_adr)
{
    cc2500_spi_enable();
    spi0_write_byte(SPI_SLAVE_ID, reg_adr | CC2500_REG_ACCESS_OP_READ | CC2500_REG_READ_BURST);
    uint8_t r = spi0_read_byte(SPI_SLAVE_ID);
    cc2500_spi_disable();
    return r;
}


static void cc2500_spi_strobe(uint8_t strobe)
{
    cc2500_spi_enable();
    spi0_write_byte(SPI_SLAVE_ID, strobe | CC2500_REG_ACCESS_OP_WRITE | CC2500_REG_ACCESS_NOBURST);
    cc2500_spi_disable();
}

static inline void cc2500_flush_tx(void)
{
    cc2500_spi_strobe(CC2500_STROBE_SFTX);
}

static void cc2500_config(const struct RF_SETTINGS_t *settings)
{
    cc2500_spi_write_reg(CC2500_REG_FSCTRL1, settings->fsctrl1);
    cc2500_spi_write_reg(CC2500_REG_FSCTRL0, settings->fsctrl0);
    cc2500_spi_write_reg(CC2500_REG_FREQ2, settings->freq2);
    cc2500_spi_write_reg(CC2500_REG_FREQ1, settings->freq1);
    cc2500_spi_write_reg(CC2500_REG_FREQ0, settings->freq0);
    cc2500_spi_write_reg(CC2500_REG_MDMCFG4, settings->mdmsettings4);
    cc2500_spi_write_reg(CC2500_REG_MDMCFG3, settings->mdmsettings3);
    cc2500_spi_write_reg(CC2500_REG_MDMCFG2, settings->mdmsettings2);
    cc2500_spi_write_reg(CC2500_REG_MDMCFG1, settings->mdmsettings1);
    cc2500_spi_write_reg(CC2500_REG_MDMCFG0, settings->mdmsettings0);
    cc2500_spi_write_reg(CC2500_REG_CHANNR, settings->channr);
    cc2500_spi_write_reg(CC2500_REG_DEVIATN, settings->deviatn);
    cc2500_spi_write_reg(CC2500_REG_FREND1, settings->frend1);
    cc2500_spi_write_reg(CC2500_REG_FREND0, settings->frend0);
    cc2500_spi_write_reg(CC2500_REG_MCSM0, settings->mcsm0);
    cc2500_spi_write_reg(CC2500_REG_FOCCFG, settings->focsettings);
    cc2500_spi_write_reg(CC2500_REG_BSCFG, settings->bssettings);
    cc2500_spi_write_reg(CC2500_REG_AGCCTRL2, settings->agcctrl2);
    cc2500_spi_write_reg(CC2500_REG_AGCCTRL1, settings->agcctrl1);
    cc2500_spi_write_reg(CC2500_REG_AGCCTRL0, settings->agcctrl0);
    cc2500_spi_write_reg(CC2500_REG_FSCAL3, settings->fscal3);
    cc2500_spi_write_reg(CC2500_REG_FSCAL2, settings->fscal2);
    cc2500_spi_write_reg(CC2500_REG_FSCAL1, settings->fscal1);
    cc2500_spi_write_reg(CC2500_REG_FSCAL0, settings->fscal0);
    cc2500_spi_write_reg(CC2500_REG_FSTEST, settings->fstest);
    cc2500_spi_write_reg(CC2500_REG_TEST2, settings->test2);
    cc2500_spi_write_reg(CC2500_REG_TEST1, settings->test1);
    cc2500_spi_write_reg(CC2500_REG_TEST0, settings->test0);
    cc2500_spi_write_reg(CC2500_REG_FIFOTHR, settings->fifothr);
    cc2500_spi_write_reg(CC2500_REG_IOCFG2, settings->iosettings2);
    cc2500_spi_write_reg(CC2500_REG_IOCFG0, settings->iosettings0d);
    cc2500_spi_write_reg(CC2500_REG_PKTCTRL1, settings->pktctrl1);
    cc2500_spi_write_reg(CC2500_REG_PKTCTRL0, settings->pktctrl0);
    cc2500_spi_write_reg(CC2500_REG_ADDR, settings->addr);
    cc2500_spi_write_reg(CC2500_REG_PKTLEN, settings->pktlen);
    cc2500_spi_write_reg(CC2500_PATABLE_ADDR, CC2500_PATABLE_VALUE);
}

void cc2500_init()
{
    SPI0->csr[0] = (0 << 0) // CPOL
                 | (1 << 1) // NCPHA
                 | (1 << 8) // SCBR, todo
                 ;

    cc2500_config(&default_settings);
}

void cc2500_send_packet(const void *buffer, size_t size)
{
    const uint8_t *buf = buffer;
    int smallpacket = (size + 1 < 256); // Additional byte for packet length

    if(smallpacket)
    {
        int first_frame = 1;
        size_t txbytes = 1;
        cc2500_flush_tx(); 
        /* Fill tx fifo /!\ variable packet length usage. */
        cc2500_spi_tx_fifo_byte(size);
        do
        {
            size_t to_write = 64-txbytes;
            to_write = (size < to_write ? size : to_write);

            cc2500_spi_tx_fifo_burst(buf, to_write);
            if(first_frame)
                cc2500_spi_strobe(CC2500_STROBE_STX);
            buf += to_write;
            size -= to_write;
            first_frame = 0;

            /* Active wait until TX fifo is half empty */
            do {
                txbytes = (cc2500_spi_read_ro_reg(CC2500_REG_TXBYTES) & 0x7f);
            } while(txbytes > 60);
        } while(size);

        do {
            txbytes = (cc2500_spi_read_ro_reg(CC2500_REG_TXBYTES) & 0x7f);
        } while(txbytes > 0);
        while((cc2500_spi_read_ro_reg(CC2500_REG_MARCSTATE) & 0x1f) != 0x01);
    }
    else
    {
    }
}

