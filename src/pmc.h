#ifndef PMC_H
#define PMC_H

#include <stdint.h>

struct pmc_t
{
    uint32_t
        scer,
        scdr,
        scsr,

        _reserved0,

        pcer0,
        pcdr0,
        pcsr0,
        ckgr_uckr,
        ckgr_mor,
        ckgr_mcfr,
        ckgr_pllar,

        _reserved1,

        mckr,

        _reserved2,

        usb,

        _reserved3,

        pck[7],

        _reserved4,

        ier,
        idr,
        sr,
        imr,
        fsmr,
        fspr,
        focr,

        _reserved5[26],

        wpmr,
        wpsr,

        _reserved6[5],

        pcer1,
        pcdr1,
        pcsr1,
        pcr,
        ocr,
        slpwk_er0,
        slpwk_dr0,
        slpwk_sr0,
        slpwk_asr0,
        pmmr,
        slpwk_er1,
        slpwk_dr1,
        slpwk_sr1,
        slpwk_asr1,
        slpwk_aipr;
};

enum pmc_main_clock_e
{
    MAIN_4MHZ = 0,
    MAIN_8MHZ = 1,
    MAIN_12MHZ = 2,
};

enum pmc_master_div_e
{
    MASTER_DIV_1 = 0,
    MASTER_DIV_2 = 1,
    MASTER_DIV_4 = 2,
    MASTER_DIV_3 = 3,
};

enum pmc_master_mcu_prescaler_e
{
    MASTER_PRES_1  = 0,
    MASTER_PRES_2  = 0,
    MASTER_PRES_4  = 0,
    MASTER_PRES_8  = 0,
    MASTER_PRES_16 = 0,
    MASTER_PRES_32 = 0,
    MASTER_PRES_64 = 0,
    MASTER_PRES_3  = 0,
};

#define PMC ((volatile struct pmc_t*) 0x400e0600)

void pmc_set_main_and_master_clocks(
    enum pmc_main_clock_e           main_freq,
    enum pmc_master_mcu_prescaler_e master_pres,
    enum pmc_master_div_e           master_div
);
/* pmc_set_main_and_master_clocks:
 * Main clock frequency (MAINCK) = main_freq
 * Processor clock (HCLK) = MAINCK / master_pres
 * Master clock (MCK) = MAINCK / (master_pres * master_div)
 */

#endif

