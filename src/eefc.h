#ifndef EEFC_H
#define EEFC_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

enum eefc_cmd_result_e
{
    OK,
    LOCK_ERROR,
    KEY_ERROR
};

#define FLASH ((uint32_t*) 0x00400000)
#define FLASH_PAGE_SIZE 512
#define FLASH_SECTOR_SIZE (128<<10)

#define GPNVM_ROM_FLASH 1

void eefc_init_for_programming();
enum eefc_cmd_result_e eefc_write_gpnvm(size_t number, bool set);
void eefc_wait();
enum eefc_cmd_result_e eefc_clear_lock(size_t page);
enum eefc_cmd_result_e eefc_erase_sector(size_t sector);
enum eefc_cmd_result_e eefc_wp(size_t page);
enum eefc_cmd_result_e eefc_write_page(size_t page, const uint32_t *src, size_t len);

#endif


