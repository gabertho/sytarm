#include "eefc.h"
#include "uart.h"

struct eefc_t
{
    uint32_t
        fmr,
        fcr,
        fsr,
        frr;
    // TODO: rest
};

enum eefc_cmd_e
{
    GETD = 0,
    WP = 1,
    WPL = 2,
    EWP = 3,
    EWPL = 4,
    EA = 5,
    EPA = 7,
    SLB = 8,
    CLB = 9,
    GLB = 10,
    SGPB = 11,
    CGPB = 12,
    GGPB = 13,
    STUI = 14,
    SPUI = 15,
    GCALB = 16,
    ES = 17,
    WUS = 18,
    EUS = 19,
    STUS = 20,
    SPUS = 21
};

#define EEFC ((volatile struct eefc_t*) 0x400e0c00)
#define FSR_FRDY   1
#define FSR_FCMDE  2
#define FSR_FLOCKE 4
#define FKEY 0x5a000000

#define PORTB_MASK (1 << 12)

static enum eefc_cmd_result_e eefc_issue_cmd(size_t cmd)
{
    eefc_wait();

    EEFC->fcr = FKEY | cmd;

    uint32_t fsr;
    do
    {
        fsr = EEFC->fsr;
    } while(!(fsr & 1));

    if(fsr & FSR_FLOCKE)
        return LOCK_ERROR;
    if(fsr & FSR_FCMDE)
        return KEY_ERROR;
    return OK;
}

void eefc_init_for_programming()
{
    *(volatile uint32_t*)0x400881E4 = 0x4d415400;
    *(volatile uint32_t*)0x40088114 &= ~PORTB_MASK;

    // Disable ERASE pin (PB12) , use it as gpio input instead
    PIOB->wpmr = 0x50494f00;
    PIOB->per = PORTB_MASK;
    PIOB->odr = PORTB_MASK;

    EEFC->fmr = (1<<16) // SCOD
              | (6 << 8) // FWS
              ;
    asm("dsb");
}

enum eefc_cmd_result_e eefc_write_gpnvm(size_t number, bool set)
{
    return eefc_issue_cmd((set ? SGPB : CGPB) | (number << 8));
}

void eefc_wait()
{
    asm("dsb");
    while(!(EEFC->fsr & 1));
}

enum eefc_cmd_result_e eefc_clear_lock(size_t page)
{
    return eefc_issue_cmd(0x9 | (page << 8)); // CLB
}

enum eefc_cmd_result_e eefc_erase_sector(size_t sector)
{
    return eefc_issue_cmd(0x11 | (sector << 8));
}

#if 0
void flash_ewp(size_t page)
{
    EEFC->fcr = 0x5A000003 | (page << 8); // EWP
    flash_wait();
}
#endif

enum eefc_cmd_result_e eefc_wp(size_t page)
{
    return eefc_issue_cmd(0x1 | (page << 8)); // WP
}

enum eefc_cmd_result_e eefc_write_page(size_t page, const uint32_t *src, size_t len)
{
    volatile uint32_t *dst = FLASH + page * (FLASH_PAGE_SIZE/sizeof(*src));

    for(size_t i = 0; i < len; ++i)
    {
        *dst++ = *src++;
        asm("dsb");
    }
    return eefc_wp(page);
}

