#include "rtt.h"
#include "pmc.h"
#include "nvic.h"

void rtt_set_periodic(uint16_t cycles)
{
    uint32_t _cycles = cycles;
    if(_cycles > 0 && _cycles < 3)
        _cycles = 3;

    RTT->mr = 0;
    RTT->mr = _cycles
            | (0 << 16) // ALMIEN
            | (1 << 17) // RTTINCIEN
            | (1 << 18) // RTTRST
            | (0 << 20) // RTTDIS
            | (0 << 24) // RTC1HZ
            ;
}

void rtt_ack_interrupt(void)
{
    volatile uint32_t sr = RTT->sr;
    (void) sr;
}

void rtt_restore(void)
{
    // Fake sytare power-loss interrupt
    rtt_set_periodic(RTT_AT_LEAST_US(1000000)); // 1 second
    rtt_ack_interrupt();
    nvic_clear(RTT_PER_ID);
    nvic_enable(RTT_PER_ID);
}

