#include "uart.h"
#include "pmc.h"
#include "led.h"
#include "gpio.h"

#define UART0_PER_ID 7

static char digit_to_char(unsigned int x)
{
    x &= 0xf;
    if(x < 10)
        return '0' + x;
    return 'a' + (x - 10);
}

void uart0_init()
{
    PIOA->wpmr = 0x50494f00;
    PIOA->pdr = (3 << 9);
    PIOA->abcdsr1 &= ~(3 << 9);
    PIOA->abcdsr2 &= ~(3 << 9);

    PMC->wpmr = 0x504d4300;
    PMC->pcer0 = (1 << UART0_PER_ID);
    PMC->pcr  = (UART0_PER_ID)
              | (1 << 8)  // Main clock (4 for master clock)
              | (1 << 12) // CMD = write
              | (1 << 28) // Enable
              ;

    UART0->wpmr = 0x55415200; // Disable write protection
    UART0->mr = (4 << 9)  // No parity
              ;
    UART0->idr = 0xffff; // Disable all interrupts
    UART0->brgr = 625;
    UART0->cr = (1<<2) // RSTRX
              | (1<<3) // RSTTX
              | (1<<4) // RXEN
              | (1<<6) // TXEN
              ;
    UART0->cr =
                (1<<4) // RXEN
              | (1<<6) // TXEN
              ;
}

void uart0_putc(char c)
{
    while(!(UART0->sr & 0x2)); // Wait till TXRDY
    UART0->thr = c;
}

void uart0_puts(const char *s)
{
    char c;
    while((c = *s++))
        uart0_putc(c);
    uart0_putc('\r');
    uart0_putc('\n');
}

void uart0_print_h(uint16_t x)
{
    char s[5];
    for(size_t i = 0; i < 4; ++i, x >>= 4)
        s[3-i] = digit_to_char(x);
    s[4] = 0;
    uart0_puts(s);
}

void uart0_print_u(uint32_t x)
{
    char s[9];
    for(size_t i = 0; i < 8; ++i, x >>= 4)
        s[7-i] = digit_to_char(x);
    s[8] = 0;
    uart0_puts(s);
}

