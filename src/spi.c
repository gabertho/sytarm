#include "spi.h"
#include "pmc.h"
#include "gpio.h"

#define SPI0_PER_ID 21
#define SPI1_PER_ID 42

#define SPI0_PORTB_MASK 0x00700000
#define SPI0_PORTD_MASK 0x00000004

#define SR_RDRF (1 << 0)
#define SR_TDRE (1 << 1)

void spi0_init()
{
    PIOB->wpmr     = 0x50494f00;
    PIOB->pdr      = SPI0_PORTB_MASK;
    PIOB->abcdsr1 |= SPI0_PORTB_MASK;
    PIOB->abcdsr2 |= SPI0_PORTB_MASK;

    PIOD->wpmr     = 0x50494f00;
    PIOD->pdr      = SPI0_PORTD_MASK;
    PIOD->abcdsr1 &= ~SPI0_PORTD_MASK;
    PIOD->abcdsr2 |= SPI0_PORTD_MASK;

    PMC->wpmr = 0x504D4300;
    PMC->pcer0 = (1 << SPI0_PER_ID);
    PMC->pcr  = (SPI0_PER_ID)
              | (1 << 8)  // Main clock (4 for master clock)
              | (1 << 12) // CMD = write
              | (1 << 28) // Enable
              ;

    SPI0->wpmr = 0x53504900;
    SPI0->cr = (1 << 0) // SPIEN
             | (1 << 16) // TXFCLR
             | (1 << 17) // RXFCLR
             | (1 << 30) // FIFOEN
             ;
    SPI0->mr = (1 << 0) // Master mode
             | (1 << 1) // PS
             | (0 << 2) // CS directly connected to device
             | (1 << 5) // WDRBT
             ;
    SPI0->idr = 0xffff;
}

void spi0_write_byte(uint8_t slave, uint8_t x)
{
    while(!(SPI0->sr & SR_TDRE));
    SPI0->tdr = (x | ((slave & 0xf) << 16));
}

uint8_t spi0_read_byte(uint8_t slave)
{
    uint8_t actual_slave;
    uint8_t ret;
    // Ignore any other slave (data is lost)
    do
    {
        while(!(SPI0->sr & SR_RDRF));
        uint32_t tmp = SPI0->rdr;
        actual_slave = ((tmp >> 16) & 0xf);
        ret = (tmp & 0xff);
    } while(actual_slave != slave);
    return ret;
}

