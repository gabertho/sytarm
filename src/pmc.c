#include "pmc.h"

#define KEY 0x00370000

void pmc_set_main_and_master_clocks(
    enum pmc_main_clock_e           main_freq,
    enum pmc_master_mcu_prescaler_e master_pres,
    enum pmc_master_div_e           master_div
)
{
    PMC->wpmr = 0x504d4300;

    while(!(PMC->sr & (1<<17)));
    PMC->ckgr_mor = (1 << 3) // MOSCRCEN
             | KEY
             ;
    while(!(PMC->sr & (1<<17)));
    PMC->ckgr_mor = (1 << 3)
             | (main_freq << 4)
             | KEY
             | (0 << 24) // MOSCSEL
             ;

    PMC->mckr = 1 // MAIN_CLK
              | (master_pres << 4) // PRES
              | (master_div << 8) // MDIV
              ;

    PMC->wpmr = 0x504d4301;
}

