#include <stdint.h>
#include <stdbool.h>

#include "services.h"
#include "../irq.h"
#include "../nvic.h"
#include "../nvram.h"
#include "../uart.h"
#include "../watchdog.h"
#include "../rtt.h"
#include "../led.h"
#include "../systimer.h"
#include "../dma.h"

#ifdef USE_MPU
#include "mpu.h"
#endif

#define _memcpy dma_fast_memcpy32

#define SYT_INVOKE_RESTORE() __asm__("svc #0")

struct syt_cpu_stack_t
{
    uint32_t
        exc_return,
        r4[8], // r4-r11
        r0[4], // r0-r3
        r12,
        lr,
        pc,
        xpsr
        ;
};

struct syt_checkpoint_t
{
    uint32_t sp;
    uint8_t checkpointing_disabled;
    uint8_t id; // 0 = checkpoints[0], 1 = checkpoints[1]
};

struct syt_driver_t
{
    void (*restore)(void);
    void (*commit)(void);
};

typedef void *syt_checkpoint_complement_t;

extern uint32_t _estack;
extern uint32_t _sdata;
extern uint32_t _ebss;
extern uint32_t _sdriver;
extern uint32_t _sizeof_stack;
extern uint32_t _sizeof_bss;
extern uint32_t _sizeof_data;
extern uint32_t _sizeof_driver;

static const void * const estack = &_estack;
static void * const sdata = &_sdata;
static void * const ebss = &_ebss;
static void * const sdriver = &_sdriver;
static const size_t sizeof_stack  = (size_t) &_sizeof_stack;
static const size_t sizeof_bss    = (size_t) &_sizeof_bss;
static const size_t sizeof_data   = (size_t) &_sizeof_data;
static const size_t sizeof_driver = (size_t) &_sizeof_driver;

static NVRAM struct syt_checkpoint_t syt_checkpoints[2] = {
    {
        .sp = ((uint32_t) estack) - sizeof(struct syt_cpu_stack_t),
        .checkpointing_disabled = false,
        .id = 0
    },
    {
        .id = 1
    }
};
static NVRAM struct syt_checkpoint_t *syt_last = &syt_checkpoints[0];
static DONT_RESTORE struct syt_checkpoint_t *syt_next;

static DONT_RESTORE syt_checkpoint_complement_t checkpoint_compl_last;
static DONT_RESTORE syt_checkpoint_complement_t checkpoint_compl_next;

static const struct syt_driver_t drivers[] = {
    { // For now, uart0.restore only performs init
        .restore = uart0_init,
        .commit = 0
    },
    {
        .restore = led_restore,
        .commit = led_commit
    },
    {
        .restore = systimer_restore,
        .commit = 0
    },
    {
        .restore = rtt_restore,
        .commit = 0
    }
};

static void syt_init(void);

int main(void)
{
    watchdog_disable();
    disable_interrupts(); // For now, mandatory when running just after
                          // programming, without having the platform
                          // unplugged prior to running kernel
    pmc_set_main_and_master_clocks(MAIN_12MHZ, MASTER_PRES_1, MASTER_DIV_1);
    dma_init();
    nvram_init();
    systimer_delay_cycles(24000); // TODO: refine this

    syt_init();

    void application_main(void);
    application_main();
}

static void syt_restore_drivers(void)
{
    const size_t ndrivers = sizeof(drivers) / sizeof(drivers[0]);
    const struct syt_driver_t *driver = &drivers[0];
    for(size_t i = ndrivers; i--; ++driver)
        driver->restore();
}

static void __attribute__((used)) syt_commit_drivers(void)
{
    const size_t ndrivers = sizeof(drivers) / sizeof(drivers[0]);
    const struct syt_driver_t *driver = &drivers[0];
    for(size_t i = ndrivers; i--; ++driver)
    {
        void (*callback)(void) = driver->commit;
        if(callback != 0)
            driver->commit();
    }
}

static void __attribute__((noreturn)) syt_restore(void)
{
    uint8_t *src = ((uint8_t*) checkpoint_compl_last) + sizeof_stack;

    // 1. Restore .data and .bss sections
    _memcpy(sdata, src, (uint8_t*) ebss - (uint8_t*) sdata);
    
    // 2. Restore drivers
    src += sizeof_bss + sizeof_data;
    _memcpy(sdriver, src, sizeof_driver); // Driver contexts
    syt_restore_drivers();                // Driver operations

#ifdef USE_MPU
    mpu_init();
#endif

    // 4. Jump to the resume-application routine in Handler mode
    enable_interrupts();
    SYT_INVOKE_RESTORE();

    // Dead code
    for(;;);
}

static inline void init_checkpoints(void)
{
    extern uint32_t _scheckpoint0;
    extern uint32_t _scheckpoint1;

    size_t index = syt_last->id;
    if(index == 1)
    {
        checkpoint_compl_last = &_scheckpoint1;
        checkpoint_compl_next = &_scheckpoint0;
    }
    else
    {
        checkpoint_compl_last = &_scheckpoint0;
        checkpoint_compl_next = &_scheckpoint1;
    }
    syt_next = syt_checkpoints + ((index & 1) ^ 1);
}

static void syt_init(void)
{
    init_checkpoints();
    syt_restore();
}

static void __attribute__((used)) syt_save_stack(const void *cpu_stack)
{
    // Save stack (CPU registers are formerly pushed onto the stack)
    size_t size = (uint8_t*) estack - (uint8_t*) cpu_stack;
    syt_next->sp = (uint32_t) cpu_stack;
    _memcpy(checkpoint_compl_next, cpu_stack, size);
}

static void __attribute__((used)) syt_create_checkpoint(void)
{
    // Assumes that syt_save_stack has already been called
    // Save .data and .bss sections
    uint8_t *dst = ((uint8_t*) checkpoint_compl_next) + sizeof_stack;
#ifdef USE_MPU
    extern const unsigned int disable_mpu;
    if(disable_mpu != 0)
        _memcpy(dst, sdata, (uint8_t*) ebss - (uint8_t*) sdata);
    else
        mpu_copy_if_dirty(dst);
#else
    _memcpy(dst, sdata, (uint8_t*) ebss - (uint8_t*) sdata);
#endif

    // Save drivers
    dst += sizeof_bss + sizeof_data;
    _memcpy(dst, sdriver, sizeof_driver);

    // Now syt_next is a valid checkpoint
}

static void __attribute__((used)) syt_swap_and_reboot(void)
{
    syt_last = syt_next;
    for(;;);
}

static void __attribute__((used)) syt_swap_and_continue(void)
{
    struct syt_checkpoint_t *tmp_checkpoint = syt_last;
    syt_last = syt_next;
    syt_next = tmp_checkpoint;

    syt_checkpoint_complement_t tmp_compl = checkpoint_compl_last;
    checkpoint_compl_last = checkpoint_compl_next;
    checkpoint_compl_next = tmp_compl;
}

void __attribute__((interrupt("IRQ"), naked)) syt_checkpoint_handler(void)
{
    __asm__(
        "push {r4-r11}\n"            // Complete IRQ stack with registers r4 to r11 (1)
        "push {lr}\n"                // Complete IRQ stack with lr register (2)
        "mov r0, sp\n"
        "cmp %0, %1\n"               // if(syt_next->checkpointing_disabled == true) -> skip syt_save_stack
        "beq _syt_checkpoint_handler_0\n"
        "bl syt_save_stack\n"        // syt_save_stack(sp)
        "_syt_checkpoint_handler_0:\n"
        "bl syt_create_checkpoint\n"
        "bl syt_swap_and_reboot\n"
                                     // Dead code
        "pop {lr}\n"                 // Mirror of (2)
        "pop {r4-r11}\n"             // Mirror of (1)
        "bx lr"
        :: "r"(syt_next->checkpointing_disabled), "r"(true)
    );
}

static void __attribute__((naked, used)) syt_resume_application(void)
{
    __asm__(
        "ldr r0, %0\n" // r0 = syt_last->sp
        "ldr r1, %1\n" // r1 = estack
        "ldr r2, =checkpoint_compl_last\n" // r2 = checkpoint_compl_last
        "ldr r2, [r2]\n"
        "mov r4, r0\n"

        // while(r0 != estack)
        // {
        "_syt_restore_stack:\n"
        "cmp r0, r1\n"
        "beq _syt_jump_to_app\n"
        "ldr r3, [r2]\n"   // r3 = *r2
        "str r3, [r0]\n"   // *r0 = r3
        "add r0, r0, #4\n" // r0 += sizeof(uint32_t)
        "add r2, r2, #4\n" // r2 += sizeof(uint32_t)
        "b _syt_restore_stack\n"
        // }
        "_syt_jump_to_app:\n"

        "mov sp, r4\n"   // sp = syt_last->sp;
        "pop {lr}\n"     // Mirror of (2)
        "pop {r4-r11}\n" // Mirror of (1)
        "bx lr"
        :
        : "m"(syt_last->sp),
          "m"(estack)
    );
}

static void __attribute__((naked, used)) syt_begin_atomic(void)
{
    syt_next->checkpointing_disabled = true;
    __asm__(
        "push {r4-r11}\n"            // Complete IRQ stack with registers r4 to r11 (1)
        "push {lr}\n"                // Complete IRQ stack with lr register (2)
        "mov r0, sp\n"
        "bl syt_save_stack\n"        // syt_save_stack(sp)
        "bl syt_create_checkpoint\n" 
        "bl syt_swap_and_continue\n"
#ifdef USE_MPU
        "bl mpu_init\n"              // Reinitialize mpu, unlock all regions again
#endif
        "pop {lr}\n"     // Mirror of (2)
        "pop {r4-r11}\n" // Mirror of (1)
        "bx lr"
   );
}

static void __attribute__((naked, used)) syt_end_atomic(void)
{
    syt_next->checkpointing_disabled = false;

    __asm__(
        "push {lr}\n"
        "bl syt_commit_drivers\n"
        "pop {lr}\n"
        "bx lr"
   );
}

void (* const svc_vector[])(void) = {
    syt_resume_application,
    syt_begin_atomic,
    syt_end_atomic
};

void __attribute__((interrupt("IRQ"), naked)) syt_svc_handler(void)
{
    __asm__(
        "ldr r0, [sp, #24]\n"  // r0 = address of pc after returning from exception
        "ldrh r0, [r0, #-2]\n" // r0 = contents of the svc instruction
        "and r0, #0xff\n"      // r0 = number passed along with svc instruction
        "cmp r0, %0\n"         // if(r0 >= sizeof(svc_vector) / sizeof(svc_vector[0])) -> return from exception
        "bmi _syt_svc_handler_0\n"
        "bx lr\n"
        "_syt_svc_handler_0:\n" // else -> branch to svc_vector[r0]
        "lsl r0, #2\n"          // r0 *= 4 (convert to offset since svc_vector entries are 32bit long)
        "ldr r1, =svc_vector\n"
        "ldr r0, [r0, r1]\n"   // r0 = svc_vector[r0]
        "bx r0\n"
        :: "i"(sizeof(svc_vector) / sizeof(svc_vector[0]))
   );
}

