#ifndef MPU_H
#define MPU_H

#include <stdint.h>
#include <stddef.h>

struct mpu_t
{
    uint32_t
        type,
        ctrl,
        rnr,
        rbar,
        rasr;
};

#define MPU ((volatile struct mpu_t*) 0xe000ed90)

#define MPU_ACCESS_RIGHTS(XN, AP, TEX, S, C, B, SRD) \
        ( (((XN)  & 0x1) << 28) \
        | (((AP)  & 0x7) << 24) \
        | (((TEX) & 0x7) << 19) \
        | (((S)   & 0x1) << 18) \
        | (((C)   & 0x1) << 17) \
        | (((B)   & 0x1) << 16) \
        | (((SRD) & 0xff) << 8) \
        )

void mpu_init(void);
void mpu_copy_if_dirty(void *base_address);

#endif

