#include <stdint.h>

#include "mpu.h"
#include "system.h"
#include "sytare/services.h"

#define N_REGIONS 16

static DONT_RESTORE uint16_t dirty_regions;

void mpu_init(void)
{
    extern const unsigned int disable_mpu;
    if(disable_mpu != 0)
        return;

    void mpu_hw_init(void);
    mpu_hw_init();

    MPU->ctrl = (1 << 0) // ENABLE
              | (0 << 0) // HFNMIENA (MPU disabled in hard fault)
              | (1 << 2) // PRIVDEFENA (default config for non-configured regions)
              ;
    __asm__(
        "dsb\n"
        "isb"
    );
    dirty_regions = 0x0000;
    system_enable_memfault();
}

void mpu_copy_if_dirty(void *base_address)
{
    void mpu_copy_region(uint32_t *base_address, size_t region);

    size_t region = 0;
    for(size_t dirty = dirty_regions; dirty; ++region, dirty >>= 1)
        if(dirty & 1)
            mpu_copy_region(base_address, region);
}

void __attribute__((interrupt("IRQ"))) syt_mpu_fault_handler(void)
{
    uint32_t fault_address = SYSTEM->mmar;
    SYSTEM->cfsr |= (1 << 1); // Acknowledge interrupt

    size_t mpu_region_of(uint32_t address);
    size_t region = mpu_region_of(fault_address);

    MPU->rnr = region; // Select region
    MPU->rasr = 0;     // Disable region
    __asm__("dsb");
    dirty_regions |= (1 << region);
}
