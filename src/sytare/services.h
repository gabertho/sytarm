#ifndef SYTARE_SERVICES_H
#define SYTARE_SERVICES_H

#include <stddef.h>

#define DONT_RESTORE __attribute__((section(".dont_restore")))
#define DRIVER       __attribute__((section(".driver")))

#define SYT_BEGIN_ATOMIC() __asm__("svc #1")
#define SYT_END_ATOMIC()   __asm__("svc #2")

#endif

