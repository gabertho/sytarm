define eefc_unprotect
    set *0x400e0ce4 = 0x45464300
end

define eefc_issue_cmd
    set $x = (0x5a000000 | $arg0)
    set *0x400e0c04 = $x
    x 0x400e0c08
end

define eefc_set_gpnvm
    set $x = (0xb | ($arg0 << 8))
    eefc_issue_cmd $x
end

define eefc_clear_gpnvm
    set $x = (0xc | ($arg0 << 8))
    eefc_issue_cmd $x
end

define eefc_print_gpnvm
    eefc_issue_cmd 0xd
    x 0x400e0c0c
end

define reconfigure_gpnvm
    eefc_unprotect
    eefc_clear_gpnvm 0
    eefc_set_gpnvm 1
    eefc_clear_gpnvm 7
    eefc_clear_gpnvm 8
    eefc_print_gpnvm
end
