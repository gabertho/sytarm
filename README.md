# sytARM

[Sytare](https://gitlab.inria.fr/citi-lab/sytare) port on ARM, get it?

The code is designed to run on the [armorik](https://github.com/gberthou/armorik) platform:
 - ATSAMS70 micro-controller (Cortex-M7).
 - FM22L16-55-TG (NVRAM, 512 kB).
 - A handful of peripherals.

## Program architecture

The `kernel` to be run on the platform needs an `initializer` to populate NVRAM memory (to store some data needed by the `kernel`).
The `initializer` runs on the SRAM while the ATMEL programmer is wired.
Once the `initializer` completes, the platform can safely be unplugged from the ATMEL programmer.
On boot, the platform will execute `kernel` from the flash memory.
Flash memory, which contains `kernel` instructions and might contain part of `kernel` data, is populated by the programmer when programming `kernel` onto the platform.

## How to compile a program

In this directory:
```
mkdir -f bin
make
```

The compilation chain does the following operations:
 1. Compile `kernel`.
 2. Dump the `kernel` `.persistent` section into a variable.
 3. Compile `initializer`, using the provided `.persistent` section.

## Very first initialization
Before following the instructions for `How to flash a program`, the micro-controller must be configure so that the Security bit is disabled (GPNVM0 bit cleared), the platform boots from Flash memory (GPNVM1 bit set) and the TCM memory is disabled (GPNVM bits 7 and 8 cleared).

In a terminal:
```
openocd -f openocd.cfg
```
At that point, you might encounter an error like this one:
```
Error: Connect failed. Consider setting up a gdb-attach event for the target to prepare target for GDB connect, or use 'gdb_memory_map disable'.
```
If you do, refer to the `Troubleshooting` section.

Then, in another terminal:
```
arm-none-eabi-gdb
> target extended-remote :3333
> monitor reset halt
> reconfigure_gpnvm
```
The `reconfigure_gpnvm` routine is defined in `.gdbinit`.
The output of this routine must look like following:
```
0x400e0c08: 0x00000001
0x400e0c08: 0x00000001
0x400e0c08: 0x00000001
0x400e0c08: 0x00000001
0x400e0c08: 0x00000001
0x400e0c0c: 0x00000042
```

Then, a hardware reboot might be necessary for the platform to take the new settings into account.

## How to program the platform

In a terminal:
```
openocd -f openocd.cfg
```

Then, in another terminal:
```
make program
```

If you don't want the plateform to run `kernel` just after programming, you can comment out `monitor reset run` in `gdb/initializer`.
Should you need a specific sequence of operations to program the platform, you might want to rewrite or modify other parts of `gdb/initializer`.

## Troubleshooting

### `gdb_memory_map disable`
While running `target extended-remote :3333` on gdb, the request might get rejected, especially if it's the first time flashing the micro-controller, with the following error:
```
Error: Connect failed. Consider setting up a gdb-attach event for the target to prepare target for GDB connect, or use 'gdb_memory_map disable'.
```
Then, uncomment the `gdb_memory_map disable` line at the end of `openocd.cfg`
