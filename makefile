APP=bin/app
SYTARE=bin/sytare
PARTIAL=bin/partial
INITIALIZER=bin/initializer

TOOLCHAIN=arm-none-eabi
CC=gcc
AS=as
LD=ld
OBJDUMP=objdump
READELF=readelf

CCFLAGS=-std=gnu11 -g -Wall -Wextra -pedantic -pedantic-errors -Werror -mcpu=cortex-m7 -mthumb -fno-builtin -O3
# Is it a bug from arm-none-eabi-gcc 10.1.0?
CCFLAGS:=$(CCFLAGS) -fno-auto-inc-dec -fno-reorder-blocks

ASFLAGS=-mcpu=cortex-m7 -mthumb

INCLUDES=-Isrc/

COMMON_C=$(wildcard src/*.c)
COMMON_S=$(wildcard src/*.s)
COMMON_O=$(patsubst %.c,%.o,$(COMMON_C)) $(patsubst %.s,%.o,$(COMMON_S))

KERNEL_COMMON_C=
KERNEL_COMMON_O=src/apps/common/boot.o $(patsubst %.c,%.o,$(KERNEL_COMMON_C)) $(COMMON_O)

INITIALIZER_COMMON_O=src/initializer/boot.o src/initializer/main.o $(COMMON_O)

ALL_APPS=$(filter-out src/apps/common, $(wildcard src/apps/*))
ALL_APPS:=$(patsubst src/apps/%,$(APP)-%,$(ALL_APPS)) $(patsubst src/apps/%,$(APP)-%-mpu,$(ALL_APPS))

LD_SRAM=src/atsams70q19-sram.ld
LD_SCRIPT=src/ldscript.ld

$(INITIALIZER)-%: $(INITIALIZER_COMMON_O) src/initializer/nvram_contents-%.o src/initializer/checkpoint_contents-%.o
	$(TOOLCHAIN)-$(LD) -o $@ $^ -T $(LD_SRAM)

$(PARTIAL)-%-mpu.o: src/apps/%/vector-mpu.o $(SYTARE)-mpu.o src/apps/%/main.o $(KERNEL_COMMON_O)
	$(TOOLCHAIN)-$(LD) -relocatable -o $@ $^ -T $(LD_SCRIPT)

$(PARTIAL)-%.o: src/apps/%/vector.o $(SYTARE).o src/apps/%/main.o $(KERNEL_COMMON_O)
	$(TOOLCHAIN)-$(LD) -relocatable -o $@ $^ -T $(LD_SCRIPT)

$(APP)-%-mpu: $(PARTIAL)-%-mpu.o src/sytare/mpu.o src/apps/%/mpu-map.o
	$(TOOLCHAIN)-$(LD) -o $@ $^ -T $(LD_SCRIPT)
	$(TOOLCHAIN)-$(OBJDUMP) -xhD $@ > $@.lst

$(APP)-%: $(PARTIAL)-%.o
	$(TOOLCHAIN)-$(LD) -o $@ $^ -T $(LD_SCRIPT)
	$(TOOLCHAIN)-$(OBJDUMP) -xhD $@ > $@.lst

program-%: gdb/initializer $(APP)-% $(INITIALIZER)-%
	$(TOOLCHAIN)-gdb -q -x $< -ex "armorik-program $*"

debug-%: gdb/initializer $(APP)-% $(INITIALIZER)-%
	$(TOOLCHAIN)-gdb -q -x $< -ex "armorik-program-and-debug $*"

src/apps/%/vector.s: src/tools/irq_gen.py
	python3 $< > $@
src/apps/%/vector-mpu.s: src/tools/irq_gen.py
	python3 $^ use_mpu > $@
src/apps/%/mpu-map.c: src/tools/mpu-map.py $(PARTIAL)-%.o
	python3 $^ > $@

src/initializer/nvram_contents-%.c: $(APP)-%
	$(TOOLCHAIN)-objcopy $< /dev/null --dump-section .persistent=$<.persistent
	python3 src/tools/persistent2array.py $<.persistent > $@
	rm $<.persistent

src/initializer/checkpoint_contents-%.c: $(APP)-%
	python3 src/tools/checkpoint_init.py $< > $@

%.o: %.c
	$(TOOLCHAIN)-$(CC) $(CCFLAGS) $(INCLUDES) -o $@ -c $<

%.o: %.s
	$(TOOLCHAIN)-$(AS) $(ASFLAGS) -o $@ -c $<

all: $(ALL_APPS)

$(SYTARE).o: src/sytare/services.c
	$(TOOLCHAIN)-$(CC) $(CCFLAGS) $(INCLUDES) -o $@ -c $<

$(SYTARE)-mpu.o: src/sytare/services.c src/sytare/mpu.c
	$(TOOLCHAIN)-$(CC) $(CCFLAGS) -DUSE_MPU $(INCLUDES) -o $@ -c $<

clean:
	rm -rf $(SYTARE).o $(SYTARE)-mpu.o $(APP)-* $(PARTIAL)-* $(INITIALIZER)-* $(KERNEL_COMMON_O) $(INITIALIZER_COMMON_O) src/apps/*/vector.s src/apps/*/vector-mpu.s src/apps/*/mpu-map.c src/apps/*/*.o src/initializer/nvram_contents-*.c src/initializer/checkpoint_contents-*.c 

# Empty targets for tab-completion purposes, it does not interfere with automake

bin/app-leds:
bin/app-leds-mpu:
bin/app-quicksort:
bin/app-quicksort-mpu:

program-leds:
program-leds-mpu:
program-quicksort:
program-quicksort-mpu:

debug-leds:
debug-leds-mpu:
debug-quicksort:
debug-quicksort-mpu:
